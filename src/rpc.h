#include <event2/event.h>
#include <event2/http.h>
#include <event2/util.h>

#ifndef SEARAFT_RPC
#define SEARAFT_RPC

//currently only HTTP transport is supported
enum rpc_proto {
    HTTP
};

struct rpc_target {
   enum rpc_proto proto;
   char *host;
   int   port;
};

//supported datatypes in rpc call
enum rpc_dt {
    RPC_INT,  //simple 64-bit integer
    RPC_REAL, //simple 64-bit double
    RPC_STRING, //ASCII string
    RPC_VECTOR, //array of data 
};

//input and output data to the rpc call
struct data_t {
    enum  rpc_dt type; 
    void *value;
    struct data_t **child; //valid for VECTOR
       //contains the type and value of VECTOR members
    int   length; //valid for VECTOR
};

//method signature
struct method_t {
    char *name;
    struct data_t **params;
    int nparams;
};

//server side method callback
typedef struct data_t *(*rpc_method_impl_t)(struct data_t *params[], int nparams,
    char **error, void *satellite_data);

//client side method callback
typedef void (* rpc_callback)(struct data_t *result, char *error, void *ctx);

struct method_entry {
    char *method_name;
    rpc_method_impl_t target_method_impl;
    void *satellite_data;
    struct method_entry *next;
};

#ifndef METHOD_LOOKUP_CACHE_SIZE
#define METHOD_LOOKUP_CACHE_SIZE 8
#endif
struct method_lookup {
    char *cached_method_name[METHOD_LOOKUP_CACHE_SIZE];
    rpc_method_impl_t cached_target_method_impl[METHOD_LOOKUP_CACHE_SIZE];
    void *cached_satellite_data[METHOD_LOOKUP_CACHE_SIZE];
    struct method_entry *head;
};

struct rpc_context {
    struct method_lookup *lookup;
    struct event_base *base;
    struct evhttp *http;
    struct evhttp_bound_socket *handle;
};

enum rpc_mode {
    SERVER,
    CLIENT,
    BOTH
};

//frees all the memory associated with given object
//and its children if any
void free_data_t(struct data_t *data);

void free_method_t(struct method_t *data);

//initializes the RPC processor
struct rpc_context *init_rpc(struct event_base *base);

int init_rpc_server(struct rpc_context *rpc_context, 
    const struct rpc_target *dest);

int init_rpc_client(struct rpc_context *rpc_context);

void deinit_rpc(struct rpc_context *context);

//makes RPC to given destination for specified method with given parameters.
//the output type should be match whats inside result->type, else its error.
//the final result of is stored in result->value or result->child whichever
//is relevant. If remote method returned error then error will be populated 
//with relevant error.
//note: free result or error after processing
//return 0 - successfull call, or the return value is RPC error code
int rpc_call(struct rpc_context *context, 
    const struct rpc_target *dest, const struct method_t *method, 
    rpc_callback client_cb, void *data);

//Registers method which can be remotely invoked. Polymorphic methods are not supported
//Parameters method_sign and output_type used to create the signature of the method, 
//  these input params will have only data_t.type or data_t.child populated (value will be ignored)
//Target_method is the method which will be invoked when remote call occurs.
//return 0 - registraion is success, or the return value is registration error code
int rpc_register(struct rpc_context *context, const char *method_name,
    rpc_method_impl_t target_method_impl, void *satellite_data); 

#endif
