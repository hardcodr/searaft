#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#if !defined(__APPLE__)
#include <malloc.h>
#endif
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "utils.h"
#include "log.h"

#define RAFT_MAX_LOG_READ (10)
#define RAFT_MAX_FILES_PER_DIR (1000)

#define DIR_PERM (S_IRWXG | S_IRWXU | S_IROTH | S_IXOTH)
#define FILE_PERM (S_IRWXG | S_IRWXU | S_IROTH)

static int numsort(const struct dirent **a, const struct dirent **b) {
    //XXX: assume index/term is integer
    return atoll((*a)->d_name) - atoll((*b)->d_name);
}

static int dot_file_filter(const struct dirent *a) {
    if(strcmp(a->d_name, ".") == 0
        || strcmp(a->d_name, "..") == 0) {
        return 0;
    }
    return 1;
}

struct log_entry_t *read_log_entry(const char *filename) {
    int fd = open(filename, O_RDONLY); 
    if(fd == -1) {
        return NULL;
    }

    uint64_t term;
    uint64_t index;
    uint64_t req_id;
    uint32_t bufsize;
    if(!read_uint64(fd, &term)
        || !read_uint64(fd, &index) 
        || !read_uint64(fd, &req_id) 
        || !read_uint32(fd, &bufsize)) {
        close(fd);
        return NULL;
    }
    
    int i = 0;
    unsigned char *buffer = (unsigned char *)malloc(bufsize);
    while(i < bufsize) {
        int nread = read(fd, buffer + i, bufsize - i);
        if(nread == -1) {
            close(fd);
            free(buffer);
            return NULL;
        }
        i += nread;
    }
    close(fd);
   
    struct log_entry_t *le = (struct log_entry_t *)malloc(
            sizeof(struct log_entry_t));
    if(le) {
        le->term = term;
        le->index = index;
        le->req_id = req_id;
        le->bufsize = bufsize;
        le->buffer = buffer;

        return le;
    }

    free(buffer);
    return NULL;
}

static int write_log_entry(const char *logfile, struct log_entry_t *le) {
#if defined(__unix__) || defined(__APPLE__)
    int fd = open(logfile, O_RDWR | O_CREAT, FILE_PERM);
#else
    int fd = open(logfile, O_RDWR | O_CREAT);
#endif

    if(fd == -1) {
        return 0;
    }

    if(!write_uint64(fd, le->term)
        || !write_uint64(fd, le->index)
        || !write_uint64(fd, le->req_id)
        || !write_uint32(fd, le->bufsize)) {
        DBG_LOG(LOG_ERROR, "error in file operation");
        return 0;
    }
    int i = 0;
    while(i < le->bufsize) {
        int nwrite = write(fd, le->buffer + i, le->bufsize - i);
        if(nwrite == -1) {
            close(fd);
            return 0;
        }
        i += nwrite;
    }
    close(fd);

    return 1;
}

static int read_log_entries(struct p_log *l, uint64_t start_index) {
    struct dirent **dentries;
    struct dirent **centries;
    int n = scandir(l->logdir, &dentries, dot_file_filter, numsort);
    if(n < 0) {
        return 0; //LOG: directory cannot be read
    }
    int success = 1;
    int i = 0;
    while(n--) {
        if(start_index != 0 && 
            atoll(dentries[n]->d_name) > (start_index / RAFT_MAX_FILES_PER_DIR)) {
            free(dentries[n]);
            continue;
        }
        char *childdir = path_join(l->logdir, dentries[n]->d_name);
        free(dentries[n]);

        if(childdir) {
            int m = scandir(childdir, &centries, dot_file_filter, numsort);
            if(m < 0) {
                success = 0;
                free(childdir);
                break;
            }

            while(m--) {
                if(start_index != 0 && atoll(centries[m]->d_name) > start_index) {
                    free(centries[m]);
                    continue;
                }
                char *childfile = path_join(childdir, centries[m]->d_name);

                free(centries[m]);

                if(!childfile) {
                    success = 0;
                    break;
                }

                struct log_entry_t *le = read_log_entry(childfile);
                struct p_log_entry_t *ple = (struct p_log_entry_t *)
                    malloc(sizeof(struct p_log_entry_t));
                if(!le || !ple) {
                    success = 0;
                    free(childfile);
                    if(le) {
                        free(le->buffer);
                        free(le);
                    }
                    break;
                }

                ple->e = le;
                ple->next = ple->prev = NULL;
                if(!l->head) {
                    l->head = l->tail = ple;
                } else {
                    l->head->prev = ple;
                    ple->next = l->head;
                    l->head = ple;
                }

                free(childfile);

                if(++i == RAFT_MAX_LOG_READ) {
                    break;
                }
            }

            if(m > 0) while(m--) free(centries[m]);
            free(centries);
            free(childdir);
        } else {
            success = 0;
        }

        if(!success || i == RAFT_MAX_LOG_READ) {
            break;
        }
    }

    if(n > 0) while(n--) free(dentries[n]);
    free(dentries);

    return success;
}

struct p_log *init_log(const char *logdir) {
    struct p_log *l = (struct p_log *)malloc(sizeof(struct p_log));
    char *_logdir = u_strdup(logdir);
    if(!l || !_logdir) {
        if(l) free(l);
        return NULL;
    }
    l->logdir = _logdir;
    l->head = l->tail = NULL;

    if(read_log_entries(l, 0)) {
        return l;
    }

    deinit_log(l); 
    return NULL;
}

struct log_entry_t *get_last_entry(struct p_log *log) {
    if(log->tail) {
        return log->tail->e;
    }
    return NULL;
}

struct p_log_entry_t *get_iterator_at(struct p_log *log, uint64_t index) {
    struct p_log_entry_t *head = log->head;
    struct p_log_entry_t *tail = log->tail;

    if(head && tail->e->index >= index && index > 0) {
        while(1) {
            uint64_t high = tail->e->index;
            uint64_t low = head->e->index;
            if(low <= index) {
                uint64_t mid = low + (high - low)/2;
                struct p_log_entry_t *i;
                if(index > mid) {
                    i = tail;
                    while(i->e->index != index) {
                        i = i->prev;
                    }
                } else {
                    i = head;
                    while(i->e->index != index) {
                        i = i->next;
                    }
                }
                return i;
            }
            if(!read_log_entries(log, low)) {
                return NULL;
            }
            tail = head;
            head = log->head;
        }
    }
    return NULL;
}


struct log_entry_t *get_log_entry_at(struct p_log *log, uint64_t index) {
    struct p_log_entry_t *i = get_iterator_at(log, index);
    if(i) {
        return i->e;
    }
    return NULL;
}

static int delete_file(const char *logdir, uint64_t index) {
    char tmpbuf[50];
    snprintf(tmpbuf, 50, "%lld/%lld", (index / RAFT_MAX_FILES_PER_DIR), index);

    char *filename = path_join(logdir, tmpbuf);
    if(0 == unlink(filename)) {
        free(filename);
        return 1;
    }
    free(filename);
    return 0;
}

struct log_entry_t *append_log_entry(struct p_log *log, 
        uint64_t term, uint64_t index, uint64_t req_id,
        unsigned char *buffer, size_t bufsize) {
    if(!log->tail && index != 1) {
        DBG_LOG(LOG_FATAL, "raft invariant failure\n");
        return NULL; //XXX: raft invariant failure, may be add asserts
    }

    struct log_entry_t *le = 0;
    struct p_log_entry_t *ple = 0;

    if(log->tail) {
        if(log->tail->e->index + 1 != index) {
            struct p_log_entry_t *i = log->tail;
            while(i->e->index != index) {
                struct p_log_entry_t *tmp = i->prev;
                if(!delete_file(log->logdir, i->e->index)) {
                    //XXX: LOG: delete file failed
                }
                free(i->e->buffer);
                free(i->e);
                free(i);
                i = tmp;
            }
            i->next = NULL;
            free(i->e->buffer);

            i->e->term = term;
            i->e->index = index;
            i->e->req_id = req_id;
            i->e->buffer = buffer;
            i->e->bufsize = bufsize;

            log->tail = i->prev;

            ple = i;
            le = ple->e;
        }
    } 

    if(!ple) {
        le = (struct log_entry_t *)
            malloc(sizeof(struct log_entry_t));
        ple = (struct p_log_entry_t *)
            malloc(sizeof(struct p_log_entry_t));
        if(!le || !ple) {
            if(le) {
                free(le);
            }
            return NULL;
        }
        le->term = term;
        le->index = index;
        le->req_id = req_id;
        le->bufsize = bufsize;
        le->buffer = buffer;

        ple->next = ple->prev = NULL;
        ple->e = le;

    }

    //XXX: move directory creation code in "write_log_entry" function
    char tmpbuf[20];
    char *storedir = NULL;
    char *logfile = NULL;
    if(index >= RAFT_MAX_FILES_PER_DIR) {
        snprintf(tmpbuf, 20, "%lld", (index / RAFT_MAX_FILES_PER_DIR));
    } else {
        snprintf(tmpbuf, 20, "0");
    }
    storedir = path_join(log->logdir, tmpbuf);
    if(storedir) {
        int exists;
        if(file_exists(storedir, &exists)) {
            if(!exists) {
#if defined(__unix__) || defined(__APPLE__)
                int r = mkdir(storedir, DIR_PERM);
#else
                int r = mkdir(storedir);
#endif
                if(r) {
                    perror("mkdir failed:");
                    goto err;
                }
            }

            snprintf(tmpbuf, 20, "%lld", index);
            logfile = path_join(storedir, tmpbuf);
            if(logfile) {
                if(!write_log_entry(logfile, le)) {
                    DBG_LOG(LOG_ERROR, "error in file operation\n");
                    goto err;
                }
                
                ple->e = le;
                ple->next = ple->prev = NULL;
                if(!log->tail) {
                    log->head = log->tail = ple;
                } else {
                    log->tail->next = ple;
                    ple->prev = log->tail;
                    log->tail = ple;
                }
                free(storedir);
                free(logfile);
                return le;
            }
        }
    }
err:
    free(le);
    free(ple);
    if(storedir)
        free(storedir);
    if(logfile)
        free(logfile);
    free(le);
    return NULL;
}


struct log_entry_t *append_log_entries(struct p_log *log, 
        struct log_entry_t **entries, int nentries) {
    struct log_entry_t *last = NULL;
    for(int i = 0; i < nentries; i++) {
        //TODO: better implementation where log_entry_t are not reallocated
        last = append_log_entry(log, entries[i]->term, entries[i]->index,
           entries[i]->req_id, entries[i]->buffer, entries[i]->bufsize);
        if(!last) {
            return NULL;
        }
    }
    //for(int i = 0; i < nentries; i++) {
    //    free(entries[i]);
    //}
    //free(entries);
    return last;
}

void deinit_log(struct p_log *log) {
    free(log->logdir);
    for(;log->head;) {
        if(log->head->e && log->head->e->buffer) 
            free(log->head->e->buffer);
        if(log->head->e)
            free(log->head->e);
        struct p_log_entry_t *next = log->head->next;
        free(log->head);
        log->head = next;
    }
    free(log);
}

#ifdef TEST_RAFT_LOG
int main(void) {
    struct p_log *l = init_log("/home/swapnil/tmp/test_searaft");
    if(l) {
#ifdef RUN_WRITE_TEST
        for(int i = 0; i < 100; i++) {
            if(!append_log_entry(l, 1, i+1, u_strdup("test"), 5)) {
                printf("failed: log writing failed for %d\n", i+1);
                return 1;
            }
        }
#else
        struct log_entry_t *le = get_last_entry(l);
        if(le) {
            if(le->term == 1 && le->index == 100 && le->bufsize == 5) {
                if(!le->buffer || 0 != strcmp(le->buffer, "test")) {
                    printf("failed: incorrect buffer in log entry\n");
                }
            } else {
                printf("failed: incorrect log entry %d\n", le->index);
            }
        } else {
            printf("failed: cant read log entry\n");
        }

        le = get_log_entry_at(l, 25);
        if(le) {
            if(le->term == 1 && le->index == 25 && le->bufsize == 5) {
                if(!le->buffer || 0 != strcmp(le->buffer, "test")) {
                    printf("failed: incorrect buffer in log entry\n");
                }
            } else {
                printf("failed: incorrect log entry\n");
            }
        } else {
            printf("failed: cant read log entry at index\n");
        }

        if(!append_log_entry(l, 1, 75, u_strdup("test"), 5)) {
            printf("failed: log writing failed for %d\n", 75);
            return 1;
        }

        struct log_entry_t **entries = (struct log_entry_t **)malloc(sizeof(struct log_entry_t *)*25);
        for(int i = 0; i < 25; i++) {
            entries[i] = (struct log_entry_t *)malloc(sizeof(struct log_entry_t));
            entries[i]->term = 1;
            entries[i]->index = 76 + i;
            entries[i]->buffer = u_strdup("test");
            entries[i]->bufsize = 5;
        }
        le = append_log_entries(l, entries, 25);
        if(le) {
            if(le->term == 1 && le->index == 100 && le->bufsize == 5) {
                if(!le->buffer || 0 != strcmp(le->buffer, "test")) {
                    printf("failed: incorrect buffer in log entry\n");
                }
            } else {
                printf("failed: incorrect log entry %d\n", le->index);
            }
        } else {
            printf("failed: cant read log entry at index\n");
        }

#endif
        deinit_log(l);
    } else {
        printf("failed: cant read log directory\n");
    }
    return 0;
}
#endif
