#include <event2/event.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <event2/bufferevent_compat.h>
#include <event2/buffer.h>
#include <event2/buffer_compat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "utils.h"
#include "client.h"
#include "raft.h"

#define CLIENT_REQ_MAXLEN (1024*10)

static const char *ss[] = { "F", "C", "L"};



unsigned char *result_to_response(int result_code, const char *buf, int *outlen) {
    int slen = strlen(buf);
    *outlen = 1 + 1 + slen;
    unsigned char *response = (unsigned char *)malloc(*outlen);
    if(!response) {
        *outlen = 0;
        return NULL;
    }

    response[0] = (unsigned char)result_code;
    response[1] = slen;
    
    if(slen != 0) {
        memmove(response + 2, buf, slen);
    }

    return response;
}

static int send_actual_response(struct bufferevent *bev, 
        uint64_t req_id, int result_code, const char *res) {
    int outlen = 0;
    unsigned char *ares = result_to_response(result_code, res, &outlen);
    if(ares) {
        bufferevent_write(bev, &req_id, 8);
        bufferevent_write(bev, ares, outlen);
        free(ares);
        return 0;
    }
    return 1;
}

static void client_connection_clenaup(struct server_context_t *s, 
        struct bufferevent *bev) {
    evutil_socket_t fd = bufferevent_getfd(bev);
    bufferevent_free(bev);


    DBG_LOG(LOG_DEBUG, "[%s][%d] Cleaning up connection for client with fd = %d"
            , ss[s->state], s->current_term, fd);

    struct client_session_t *sitr = s->client_handler->clients_head;
    struct client_session_t *prev = 0;
    while(sitr) {
        if(sitr->fd == fd) {
            //remove all the requests
            if(sitr->requests_head) {
                struct client_req_t *ritr = sitr->requests_head;
                while(ritr) {
                    struct client_req_t *tmp = ritr;
                    dictDelete(s->client_handler->request_to_fd, &tmp->req_id);
                    free(tmp);
                    ritr = ritr->next;
                }
            }
            break;
        }
        prev = sitr;
        sitr = sitr->next;
    }
    if(sitr) {
        if(prev) {
            prev->next = sitr->next;
        } else {
            s->client_handler->clients_head = sitr->next;
        }
        if(!sitr->next) {
            s->client_handler->clients_tail = prev;
        }
        free(sitr);
    }
}

static void enqueue_request(struct server_context_t *s, 
        struct bufferevent *bev, uint64_t req_id) {
    evutil_socket_t fd = bufferevent_getfd(bev);
    struct client_session_t *ses = s->client_handler->clients_head;

    DBG_LOG(LOG_DEBUG, "[%s][%d] Enqueing request for client with fd = %d"
            , ss[s->state], s->current_term, fd);
    while(ses) {
        if(ses->fd == fd) {
            break;
        }
        ses = ses->next;
    }

    if(!ses) {
        ses = (struct client_session_t *)
            malloc(sizeof(struct client_session_t));
        if(s->client_handler->clients_tail) {
            s->client_handler->clients_tail->next = ses;
        } else {
            s->client_handler->clients_head = ses;
        }
        s->client_handler->clients_tail = ses;
        ses->fd = fd;
        ses->bev = bev;
        ses->requests_head = 0;
        ses->requests_tail = 0;
        ses->next = 0;
    }

    struct client_req_t *req = (struct client_req_t *)
        malloc(sizeof(struct client_req_t));
    req->req_id = req_id;
    req->next = 0;

    if(ses->requests_tail) { 
        ses->requests_tail->next = req;
    } else {
        ses->requests_head = req;
    }
    ses->requests_tail = req;

    if(DICT_OK != dictAdd(s->client_handler->request_to_fd, &req_id, &fd)) {
        DBG_LOG(LOG_DEBUG, "[%s][%d] dictAdd failed for req_id = %llu"
            , ss[s->state], s->current_term, req_id);
    }
}

static void read_callback(struct bufferevent *bev, void *ctx) {
    struct server_context_t *s = (struct server_context_t *)ctx;

    struct evbuffer *buf = EVBUFFER_INPUT(bev);
    size_t buffer_len = EVBUFFER_LENGTH(buf);
    ev_uint32_t record_len;

    if (buffer_len < 4) {
        return;
    }

    evbuffer_copyout(buf, &record_len, 4);

    record_len = ntohl(record_len);

    if(buffer_len < record_len + 4) {
        return;
    }

    evbuffer_drain(buf, 4);

    uint32_t client_id;
    uint32_t req_id;
    unsigned char *record;
    int payload_len = record_len - (2*4);

    //read the header
    evbuffer_copyout(buf, &client_id, 4);
    client_id = ntohl(client_id);
    evbuffer_drain(buf, 4);

    evbuffer_copyout(buf, &req_id, 4);
    req_id = ntohl(req_id);
    evbuffer_drain(buf, 4);

    uint64_t combined_req_id = client_id;
    combined_req_id = (combined_req_id << 32) + req_id;

    DBG_LOG(LOG_DEBUG, "[%s][%d] Request from client = %llu."
            , ss[s->state], s->current_term, combined_req_id);

    if(record_len >= CLIENT_REQ_MAXLEN) {
        //if client sends request, just
        //drain the buffer and ignore the request
        evbuffer_drain(buf, record_len + 4);

        char response[100];
        snprintf(response, 100, "Request too long. MAXLEN = %d", CLIENT_REQ_MAXLEN);
        if(send_actual_response(bev, combined_req_id, 
                    INCORRECT_REQUEST_ERROR, response)) {
            client_connection_clenaup(s, bev);
        }
        return;
    }

   //read the payload
    record = (unsigned char *)malloc(payload_len);
    if (!record) {
        DBG_LOG(LOG_WARN, "[%s][%d] client-%d:req_id-%d failed out of memory"
                , ss[s->state], s->current_term, client_id, req_id);
        return;
    }
    evbuffer_remove(buf, record, payload_len);

    uint64_t log_index = -1;
    if(s->state == LEADER) {
        log_index = log_and_send_append_entries(s, combined_req_id, record, payload_len);
    } else {
        DBG_LOG(LOG_WARN, "[%s][%d] client-%d:req_id-%d failed not leader"
                , ss[s->state], s->current_term, client_id, req_id);

        struct peer_t *leader = 0;
        for(int i = 0; i < s->quoram_size - 1; i++) {
            if(s->peers[i]->id == s->current_leader) {
                leader = s->peers[i];
                break;
            }
        }

        if(leader) {
            char response[100];
            snprintf(response, 100, "%s:%d", 
                    leader->dest->host, leader->dest->port);
            if(send_actual_response(bev, combined_req_id, REDIRECT, response)) {
                client_connection_clenaup(s, bev);
            }
        } else {
            if(send_actual_response(bev, combined_req_id, 
                        INTERNAL_ERROR, "Leader not elected")) {
                client_connection_clenaup(s, bev);
            }
        }
        return;
    }

    /*
    if(s->client_handler->active_conns >= MAX_CLIENT_CONNS) {
        char response[100];
        snprintf(response, 100, "Connection threshold reached. MAXCONNS = %d"
                , MAX_CLIENT_CONNS);
        if(send_actual_response(bev, combined_req_id, 
                    THROTTLING_ERROR, response)) {
            client_connection_clenaup(s, bev);
        }
        return;
    }
    */

    enqueue_request(s, bev, combined_req_id);
}

static void write_callback(struct bufferevent *bev, void *ctx) { 
}

static void event_callback(struct bufferevent *bev, short events, void *ctx) { 
    struct server_context_t *s = (struct server_context_t *)ctx;
    if(events & (BEV_EVENT_ERROR | BEV_EVENT_EOF)) {
        client_connection_clenaup(s, bev);
    }
}

//TODO: improve code struture: encoding of result code spread across 
//client handler and STM.
void send_client_response(struct server_context_t *s, 
        uint64_t req_id, void *res, int reslen) {
    dictEntry *de = dictFind(s->client_handler->request_to_fd, &req_id);
    if(de) {
        evutil_socket_t fd = *((evutil_socket_t *)de->val);
        DBG_LOG(LOG_DEBUG, "[%s][%d] Sending response for client with fd = %d"
                , ss[s->state], s->current_term, fd);

        struct client_session_t *ses = s->client_handler->clients_head;
        while(ses) {
            if(ses->fd == fd) {
                break;
            }
            ses = ses->next;
        }
        assert(ses != NULL); 

        bufferevent_write(ses->bev, &req_id, 8);
        bufferevent_write(ses->bev, res, reslen);
    
        struct client_req_t *ritr = ses->requests_head;
        struct client_req_t *prev = 0;
        while(ritr) {
            if(ritr->req_id == req_id) {
                break;
            }
        }
        if(ritr) {
            if(prev) {
                prev->next = ritr->next;
            } else {
                ses->requests_head = ritr->next;
            }
            if(!ritr->next) {
                ses->requests_tail = prev;
            }
            free(ritr);
        }
        dictDelete(s->client_handler->request_to_fd, &req_id);
    }
}

static void accept_callback(struct evconnlistener *listener,
    evutil_socket_t fd, struct sockaddr *address, int socklen, void *ctx) {

    struct event_base *base = evconnlistener_get_base(listener);
    struct bufferevent *bev = bufferevent_socket_new(
            base, fd, BEV_OPT_CLOSE_ON_FREE);

    bufferevent_setcb(bev, read_callback, write_callback, event_callback, ctx);

    bufferevent_enable(bev, EV_READ|EV_WRITE);
}

static void accept_error_callback(struct evconnlistener *listener, void *ctx) {
    struct event_base *base = evconnlistener_get_base(listener);
    struct server_context_t *s = (struct server_context_t *)ctx;

    int err = EVUTIL_SOCKET_ERROR();
    DBG_LOG(LOG_FATAL,"[%s][%d] Error %d (%s) on the listener. Shutting down."
            , ss[s->state], s->current_term, err
            , evutil_socket_error_to_string(err));

    event_base_loopexit(base, NULL);
}

static unsigned int clientHash(const void *_key) {
    //hash6432shift https://gist.github.com/badboy/6267743
    uint64_t key = *((uint64_t *)_key);
    key = (~key) + (key << 18); // key = (key << 18) - key - 1;
    key = key ^ (key >> 31);
    key = key * 21; // key = (key + (key << 2)) + (key << 4);
    key = key ^ (key >> 11);
    key = key + (key << 6);
    key = key ^ (key >> 22);
    return (unsigned int)key;
}

static void *clientKeyDup(void *privdata, const void *src) {
    ((void) privdata);
    uint64_t *key = (uint64_t *)malloc(sizeof(int));
    *key = *((uint64_t *)src);
    return key;
}


static void *clientValDup(void *privdata, const void *src) {
    ((void) privdata);
    evutil_socket_t *fd = (evutil_socket_t *)malloc(sizeof(evutil_socket_t));
    *fd = *((evutil_socket_t *)src);
    return fd;
}

static int clientKeyCompare(void *privdata, const void *key1, const void *key2) {
    ((void) privdata);

    uint64_t l1 = *((uint64_t *)key1);
    uint64_t l2 = *((uint64_t *)key2);
    if (l1 != l2) return 0;
    return 1;
}

static void clientKeyDestructor(void *privdata, void *key) {
    ((void) privdata);
    free(key);
}

static void clientValDestructor(void *privdata, void *val) {
    ((void) privdata);
    free(val);
}

static dictType clientRequestDict = {
    clientHash,
    clientKeyDup,
    clientValDup,
    clientKeyCompare,
    clientKeyDestructor,
    clientValDestructor
};

int start_client_handler(struct server_context_t *s, int clientport) {
    struct evconnlistener *listener;
    struct sockaddr_in sin;
    struct event_base *evbase = s->base;

    s->client_handler = (struct client_handler_t *)
        malloc(sizeof(struct client_handler_t));
    s->client_handler->request_to_fd = dictCreate(&clientRequestDict, NULL);
    s->client_handler->clients_head = 0;
    s->client_handler->clients_tail = 0;

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(0);
    sin.sin_port = htons(clientport);

    listener = evconnlistener_new_bind(
        evbase, accept_callback, s, LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE,
        -1, (struct sockaddr*)&sin, sizeof(sin));

    if(!listener) {
        return 1;
    }
    evconnlistener_set_error_cb(listener, accept_error_callback);

    return 0;
}


int stop_client_handler(struct server_context_t *s) {
    //TODO: call close on each connection in client_session_t
    dictRelease((dict *)s->client_handler->request_to_fd);
    return 0;
}
