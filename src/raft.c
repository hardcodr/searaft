#include <event2/event.h>
#if !defined(__APPLE__)
#include <malloc.h>
#endif
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>

#include "raft.h"
#include "rpc.h"
#include "methods.h"
#include "utils.h"
#include "stm.h"
#include "client.h"

#define APPEND_ENTRIES_BATCH_SIZE (100)
#define RANDOMIZATION_RANGE (1000000)

#define RAFT_LOGDIR "log"
#define RAFT_STATE  "raft.state"
#define FILE_PERM   (S_IRWXG | S_IRWXU | S_IROTH)

static const char *ss[] = { "F", "C", "L"};

static void log_fatal_and_exit(struct server_context_t *s, char *msg) {
    DBG_LOG(LOG_FATAL,"[%s][%d] %s. Shutting down", ss[s->state], s->current_term, msg);
    event_base_loopexit(s->base, NULL);
}

static void save_state(struct server_context_t *s) {
    //TODO: implement atomic operation
    DBG_LOG(LOG_INFO, "[%s][%d] Saving state to disk", ss[s->state], s->current_term);
    char *raftstate = path_join(s->basedir, RAFT_STATE);
    if(raftstate) {
#if defined(__unix__) || defined(__APPLE__)
        int fd = open(raftstate, O_RDWR | O_CREAT, FILE_PERM);
#else
        int fd = open(raftstate, O_RDWR | O_CREAT);
#endif
        if(fd == -1 || !write_uint64(fd, s->current_term)
                || !write_uint32(fd, s->current_leader)) {
            log_fatal_and_exit(s, "error in saving state to file");
        }
        close(fd);
        free(raftstate);
    }
}

static void load_state(struct server_context_t *s) {
    DBG_LOG(LOG_INFO, "[%s][%d] Loading state from disk", ss[s->state], s->current_term);
    char *raftstate = path_join(s->basedir, RAFT_STATE);
    if(raftstate) {
        int fd = open(raftstate, O_RDONLY);
        if(fd == -1) {
            return;
        }

        if(!read_uint64(fd, &s->current_term)
                || !read_uint32(fd, (uint32_t *)&s->current_leader)) {
            DBG_LOG(LOG_FATAL, "error in reading state from file");
            exit(1);
        }
        close(fd);
        free(raftstate);
    }
}

static int reset_timers(struct server_context_t *s, bool add_el, bool add_hb) {
    event_del(s->timer_el);
    event_del(s->timer_hb);

    if(add_el) {
        //randomize the election timeout
        struct timeval *et_timeval = 
            time_in_timeval(s->election_timeout_ + 
                    (random() % RANDOMIZATION_RANGE));
        if(event_add(s->timer_el, et_timeval)) {
            free(et_timeval);
            log_fatal_and_exit(s, "Election timer registration failed");
            return 1;
        }
        free(et_timeval);
    }
    if(add_hb) {
        if(event_add(s->timer_hb, s->heartbeat_timeout)) {
            log_fatal_and_exit(s, "Heartbeat timer registration failed");
            return 1;
        }
    }

    DBG_LOG(LOG_DEBUG, "[%s][%d] timer reset done", ss[s->state], s->current_term);
    return 0;
}

static void set_term(struct server_context_t *s, uint64_t term) {
    if(s->state == LEADER) {
        //if currently is leader and stepping down to follower
        //then remove heartbeat timer and add election timer
        reset_timers(s, true, false);
    }
    s->current_term = term;
    s->state = FOLLOWER;
}

/*
 * This function could be called periodically, 
 * until log_last_applied < s->commit_index
 */
void commit_unapplied_entries(struct server_context_t *s, uint64_t commit_index) {
    //apply commited changes to state machine until commit_index
    if(s->commit_index < commit_index) {
        uint64_t last_index = s->log->tail ? log_index(s->log->tail) : 0;
        s->commit_index = commit_index <= last_index ? commit_index : last_index;
    }
    DBG_LOG(LOG_DEBUG, "[%s][%d] Last Applied = %d, Commit Index = %d", 
            ss[s->state], s->current_term, s->log_last_applied, s->commit_index);
    
    if(s->log_last_applied < s->commit_index) {
        DBG_LOG(LOG_INFO, "[%s][%d] Commit unapplied entries", 
                ss[s->state], s->current_term);
        struct p_log *log = s->log;
        //get the next entry after last commit
        struct p_log_entry_t *iter = get_iterator_at(s->log, s->log_last_applied + 1);
        if(iter) {
            while(iter && log_index(iter) <= commit_index) {
                int outlen = 0;
                void *r = stm_apply(s->stm, 
                        (void *)iter->e->buffer, iter->e->bufsize, &outlen);
                if(r) {
                    if(s->state == LEADER) {
                        send_client_response(s, iter->e->req_id, r, outlen);
                    } else {
                        //TODO: send response that its no more client
                    }
                    free(r);
                } else {
                    //TODO: what to do with STM error? crash? 
                    //let's just break for now!
                    break;
                }
                s->log_last_applied = log_index(iter);
                iter = iter->next;
            }
        } 
    }
}

struct data_t *handle_append_entries(struct data_t *params[], int nparams, char **err, void *data) {
    struct server_context_t *s = (struct server_context_t *)data;
    struct append_entries_input_t *input = get_append_entries_input_params(params, nparams);
    if(!input) {
        *err = u_strdup("error while parsing input parameters");
        log_fatal_and_exit(s, 
            "AppendEntries: Error while parsing input parameters");
        return NULL;
    }
    int is_fatal = 0;
    //append entries logic
    struct append_entries_output_t output = { 0, 0 };
    if(input->term < s->current_term) {
        //leader has older term hence reject request
        output.term = s->current_term;
        output.success = 0;
    } else {
        //reset the election timeout; no point in adding heartbeat timer since
        //this is follower 
        reset_timers(s, true, false);

        int should_save_state = 0;
        if(s->current_term < input->term) {
            set_term(s, input->term);
            should_save_state = 1;
        } 
        //else if(s->state != FOLLOWER) {
        //    s->state = FOLLOWER;
        //}
        if(s->current_leader != input->leader_id) {
            s->current_leader = input->leader_id;
            should_save_state = 1;
            DBG_LOG(LOG_INFO, "[%s][%d] new leader = %d", 
                    ss[s->state], s->current_term, s->current_leader);
        }

        if(should_save_state) {
            save_state(s);
        }

        if(!s->last_entry) {
            if(input->prev_log_index == 0) {
                if(input->nentries > 0) {
                    struct log_entry_t *last =
                        append_log_entries(s->log, input->entries, input->nentries);
                    if(!last) {
                        is_fatal = 1;
                        *err = u_strdup("memory allocation failed");
                        log_fatal_and_exit(s, "AppendEntries: Memory allocation failed");
                        goto fatal_exit;
                    }
                    s->last_entry = last;
                }
                output.term = s->current_term;
                output.success = 1;
            } else {
                //wait until leader readjusts its match-index
                //to point to 0
                DBG_LOG(LOG_INFO, "[%s][%d] index readjustment %llu -> 0",
                        ss[s->state], s->current_term, input->prev_log_index);
                output.term = s->current_term;
                output.success = 0;
            }
        } else {
            if(s->last_entry->index < input->prev_log_index) {
                //wait until leader readjusts its match-index
                //to point to last entries index
                output.term = s->current_term;
                output.success = 0;
                DBG_LOG(LOG_INFO, "[%s][%d] index readjustment %llu -> %llu",
                        ss[s->state], s->current_term, input->prev_log_index,
                        s->last_entry->index);
            } else {
                uint64_t i = s->last_entry->index;
                struct log_entry_t *le = s->last_entry;
                for(;;) {
                    if(le->index == input->prev_log_index) {
                        if(le->term != input->prev_log_term) {
                            //prev_log_term didnt match with 
                            //corresponding index (§5.3)
                            output.term = s->current_term;
                            output.success = 0;
                            DBG_LOG(LOG_INFO, "[%s][%d] term readjustment %llu -> %llu at index = %llu",
                                    ss[s->state], s->current_term,
                                    input->prev_log_term, le->term, le->index);
                        } else {
                            if(input->nentries > 0) {
                                //delete entries after prev_log_term 
                                //and append new entries to match entries
                                //to the server log (§5.3)
                                struct log_entry_t *last =
                                    append_log_entries(s->log, 
                                            input->entries, input->nentries);
                                if(!last) {
                                    is_fatal = 1;
                                    *err = u_strdup("memory allocation failed");
                                    log_fatal_and_exit(s, 
                                            "AppendEntries: Memory allocation failed");
                                    goto fatal_exit;
                                }
                                s->last_entry = last;
                            }
                            output.term = s->current_term;
                            output.success = 1;
                        }
                        break;
                    } 
                    i--;
                    if(i > 0) {
                        le = get_log_entry_at(s->log, i);
                    } else {
                        break;
                    }
                }
                if(i == 0) {
                    //LOG: fatal and impossible scenario where there are some 
                    //entries present but the index never coverages to server's
                    is_fatal = 1;
                    *err = u_strdup("raft invarient failed");
                    log_fatal_and_exit(s, "AppendEntries: Raft invarient failed");
                    goto fatal_exit;
                }
            }
        }
    }
    commit_unapplied_entries(s, input->leader_commit_index);

fatal_exit:
    if(!output.success && input->nentries) {
        //XXX: this is memory leak
        //for(int i = 0; i < input->nentries; i++) {
        //    free(input->entries[i]->buffer);
        //    free(input->entries[i]);
        //}
        free(input->entries);
    }
    free(input);

    if(is_fatal) {
        return NULL;
    } else {
        return make_append_entries_rpc_response(&output);
    }
}

struct data_t *handle_request_vote(struct data_t *params[], int nparams, char **err, void *data) {
    struct server_context_t *s = (struct server_context_t *)data;
    struct request_vote_input_t *input = get_request_vote_input_params(params, nparams);
    if(!input) {
        *err = u_strdup("error while parsing input parameters");
        log_fatal_and_exit(s, 
            "RequestVote: error while parsing input parameters");
        return NULL;
    }

    struct request_vote_output_t output;
    if(input->term < s->current_term) {
        //candidate has older term hence reject request`
        output.term = s->current_term;
        output.vote_granted = 0;
    } else {
        if(s->last_entry &&
            (input->last_log_term < s->last_entry->term
                || (input->last_log_term == s->last_entry->term &&
                    input->last_log_index < s->last_entry->index))) {
            //candidate log is not up-to-date $5.4.1
            //candidate is not up-to-date if its last 
            //logs term is older or if they are equal 
            //then this server has longer log 
            output.term = s->current_term;
            output.vote_granted = 0;
        } else {
            if(input->term > s->current_term) {
                set_term(s, input->term);
                s->voted_for = input->candidate_id;
                save_state(s);
                output.term = s->current_term;
                output.vote_granted = 1;
            } else if(s->voted_for == 0) {
                //terms are equal mark the candidate
                //and this server didnt vote in this
                //election
                s->voted_for = input->candidate_id;
                save_state(s);
                output.term = s->current_term;
                output.vote_granted = 1;
            } else {
                //candidate has already voted in this term
                output.term = input->term;
                output.vote_granted = 0;
            }
        }
    }

    free(input);

    return make_request_vote_rpc_response(&output);
}

struct append_entries_cb_data_t {
    struct server_context_t *s;
    int index;
    uint64_t first_index;
    uint64_t last_index;
};

static void append_entries_cb(struct data_t *result, char *err, void *data) {
    /* TODO: why we need to create and then free the data, cant we just
     * reuse the slots, especially in case of heartbeat message? */
    struct append_entries_cb_data_t *cb_data = 
            (struct append_entries_cb_data_t *)data;
    struct server_context_t *s = cb_data->s;
    int peer_index = cb_data->index;
    uint64_t first_index = cb_data->first_index;
    uint64_t last_index = cb_data->last_index;
    
    free(cb_data);

    if(err) {
        //LOG: fatal error. TODO: exit?
        DBG_LOG(LOG_DEBUG, "[%s][%d] FAIL append_entries_cb from peer_id = %d with err = %s"
                , ss[s->state], s->current_term, peer_index, err);
        return;
    }

    struct append_entries_output_t *res = get_append_entries_output(result);
    if(!res) {
        log_fatal_and_exit(s, 
            "AppendEntriesCallback: Response parsing failed");
        return;
    }
    uint64_t pair_term = res->term;
    int pair_success = res->success;
    free(res);
    if(pair_term > s->current_term) {
        set_term(s, pair_term);
        s->voted_for = 0;
        save_state(s);
        return;
    }

    if(s->state != LEADER) {
        return;
    } 
    
    if(!pair_success) {
        DBG_LOG(LOG_DEBUG, "[%s][%d] FAIL append_entries_cb from peer_id = %d"
                , ss[s->state], s->current_term, peer_index);
        //TODO: send the client prev entry
        s->next_index[peer_index] = first_index - 1;
    } else {
        DBG_LOG(LOG_DEBUG, "[%s][%d] OK append_entries_cb from peer_id = %d"
                , ss[s->state], s->current_term, peer_index);
        s->next_index[peer_index] = last_index + 1;
        s->match_index[peer_index] = last_index;

        //If there exists an N such that N > commitIndex, a majority
        //of matchIndex[i] ≥ N, and log[N].term == currentTerm: 
        //set commitIndex = N (§5.3, §5.4).
        if(s->match_index[peer_index] > s->commit_index) {
            int nOK[s->quoram_size - 1];
            for(int i = 0; i < s->quoram_size - 1; i++) {
                nOK[i] = 0;
            }
            for(int i = 0; i < s->quoram_size - 1; i++) {
                for(int j = 0; j < s->quoram_size - 1; j++) {
                    if(s->match_index[i] <= s->match_index[j]) {
                        nOK[i]++;
                    }
                }
            }
            int majority = (s->quoram_size/2);
            uint64_t N = s->commit_index;
            int minOKs = s->quoram_size;
            for(int i = 0; i < s->quoram_size - 1; i++) {
                if(nOK[i] >= majority && nOK[i] < minOKs) {
                    minOKs = nOK[i];
                    if(s->match_index[i] > N) {
                        N = s->match_index[i];
                    }
                }
            }
            if(N > s->commit_index) {
                struct log_entry_t *entryN = get_log_entry_at(s->log, N);
                //never commit entry from previous term (§5.4.2)
                if(entryN && (entryN->term == s->current_term)) {
                    s->commit_index = N;
                    commit_unapplied_entries(s, N);
                }
            }
        }
    }
    if(s->last_entry && s->last_entry->index >= s->next_index[peer_index]) {
        //send all the entries since next_index till some threshold
        //to this server
        if(send_append_entries(s, s->next_index[peer_index], peer_index)) {
            //LOG: fatal error. TODO: exit?
        }
    }
}

int send_append_entries(struct server_context_t *s, 
        uint64_t start_index, int peer_index) {
    DBG_LOG(LOG_DEBUG, "[%s][%d] Sending append entries to peer_id = %d"
            , ss[s->state], s->current_term, peer_index);

    if(s->state != LEADER) {
        return 1;
    }

    struct append_entries_input_t *input = (struct append_entries_input_t *)
        malloc(sizeof(struct append_entries_input_t));
    if(!input) {
        return 1;
    }
    input->term = s->current_term;
    input->leader_id = s->id;
    if(start_index == 1) {
        input->prev_log_index = 0;
        input->prev_log_term = 0;
    } else {
        struct log_entry_t *prev = get_log_entry_at(s->log, start_index - 1);
        input->prev_log_index = start_index - 1;
        input->prev_log_term = prev->term;
    }
    input->leader_commit_index = s->commit_index;
    
    uint64_t last_index = s->last_entry->index;
    int possible_entries = (last_index - start_index + 1);
    if(possible_entries < APPEND_ENTRIES_BATCH_SIZE) {
        input->nentries = possible_entries;
    } else {
        input->nentries = APPEND_ENTRIES_BATCH_SIZE;
    }
    input->entries = (struct log_entry_t **)malloc(
            (input->nentries)*sizeof(struct log_entry_t *));
    if(!input->entries) {
        free(input);
        return 1;
    }
    struct p_log_entry_t* itr = get_iterator_at(s->log, start_index);
    for(int i = 0; i < input->nentries; i++, itr = itr->next) {
        input->entries[i] = itr->e;
        last_index = input->entries[i]->index;
    }

    struct method_t *rpc = make_append_entries_rpc_method(input);
    if(!rpc) {
        free(input->entries);
        free(input);
        return 1;
    }
    int res = 0;
    struct append_entries_cb_data_t *data =
        (struct append_entries_cb_data_t *)malloc(
                sizeof(struct append_entries_cb_data_t));
    if(data) {
        data->s = s;
        data->index = peer_index;
        data->first_index = start_index;
        data->last_index = start_index + input->nentries - 1;
        if(rpc_call(s->rpc_c, s->peers[peer_index]->dest, 
                    rpc, append_entries_cb, data)) {
            //just log warning, as peers might go down
            DBG_LOG(LOG_WARN, "[%s][%d] RPC call failed for peer %d", 
                    ss[s->state], s->current_term, s->peers[peer_index]->id);
        } else {
            evutil_gettimeofday(&s->last_heartbeat_at[peer_index], NULL);
        }
    } else {
        res = 1;
    }

    free(input->entries);
    free(input);
    free_method_t(rpc);
    return res;
}

uint64_t log_and_send_append_entries(struct server_context_t *s, uint64_t req_id, unsigned char *buf, size_t bufsize) {
    DBG_LOG(LOG_DEBUG, "[%s][%d] logging request for client with req_id = %llu"
            , ss[s->state], s->current_term, req_id);

    assert(s->state == LEADER);

    struct log_entry_t *prev = get_last_entry(s->log);

    uint64_t cur_index = 1;
    if(prev) {
        cur_index = prev->index + 1;
    } //else this is first entry hence cur_index = 1

    struct log_entry_t *cur = 
        append_log_entry(s->log, s->current_term, cur_index, req_id, buf, bufsize);
    if(!cur) {
        return -1;
    }
    s->last_entry = cur;

    for(int i = 0; i < s->quoram_size - 1; i++) {
        send_append_entries(s, cur_index, i);
    }
    return cur_index;
}

static int send_heartbeat(struct server_context_t *s) {
    struct append_entries_input_t *input = (struct append_entries_input_t *)
        malloc(sizeof(struct append_entries_input_t));
    if(!input) {
        return 1;
    }
    input->term = s->current_term;
    input->leader_id = s->id;
    if(s->last_entry) {
        input->prev_log_index = s->last_entry->index;
        input->prev_log_term = s->last_entry->term;
    } else {
        input->prev_log_index = 0;
        input->prev_log_term = 0;
    }
    input->leader_commit_index = s->commit_index;
    input->nentries = 0;
    input->entries = NULL;

    struct method_t *rpc = make_append_entries_rpc_method(input);
    if(!rpc) {
        free(input);
        return 1;
    }

    struct timeval current_tv;
    evutil_gettimeofday(&current_tv, NULL);
    unsigned long current_time = time_in_micros(&current_tv);

    int res = 0;
    for(int i = 0; i < s->quoram_size - 1; i++) {
        unsigned long last_timeout = time_in_micros(&s->last_heartbeat_at[i]);
        unsigned long timeout_diff = current_time - last_timeout;
        if(timeout_diff < s->heartbeat_timeout_) {
            continue;
        }
        DBG_LOG(LOG_DEBUG, "[%s][%d] Timeout since last append entry = %llu. Sending heartbeat to peer_id = %d", 
                ss[s->state], s->current_term, timeout_diff, i);

        struct append_entries_cb_data_t *data =
            (struct append_entries_cb_data_t *)malloc(
                    sizeof(struct append_entries_cb_data_t));
        if(data) {
            data->s = s;
            data->index = i;
            data->first_index = input->prev_log_index;
            data->last_index = input->prev_log_index;
            if(rpc_call(s->rpc_c, s->peers[i]->dest, rpc, 
                        append_entries_cb, data)) {
                //just log warning, as peers might go down
                DBG_LOG(LOG_WARN, "[%s][%d] RPC call failed for peer %d", 
                        ss[s->state], s->current_term, s->peers[i]->id);
            } else {
                evutil_gettimeofday(&s->last_heartbeat_at[i], NULL);
            }
        } else {
            res = 1;
        }
    }
    free(input);
    free_method_t(rpc);
    return res;
}

static void request_vote_cb(struct data_t *result, char *err, void *data) {
    struct server_context_t *s = (struct server_context_t *)data;
    if(err) {
        //what should we do with bysentine errors?
        //LOG: fatal error. TODO: exit?
        return;
    }
    if(s->state != CANDIDATE) {
        return;
    }
    struct request_vote_output_t *vote = get_request_vote_output(result);
    if(!vote) {
        log_fatal_and_exit(s, 
            "RequestVoteCallback: Response parsing failed");
        return;
    }
    int pair_vote = vote->vote_granted;
    uint64_t pair_term = vote->term;
    free(vote);
    if(pair_vote && pair_term == s->current_term
        && s->state == CANDIDATE) {
        s->current_votes++;
    } else {
        if(pair_term > s->current_term) {
            set_term(s, pair_term);
            s->voted_for = 0;
            save_state(s);
        }
        return;
    }

    int votes_needed = s->quoram_size/2 + 1;
    if(s->current_votes == votes_needed) {
        s->state = LEADER;
        s->current_leader = s->id;
        DBG_LOG(LOG_INFO, "[%s][%d] Server is elected as the leader", ss[s->state], s->current_term);
        save_state(s);
        //reset timer such that election timeout does not occur 
        //and heartbeat timeout occurs
        reset_timers(s, false, true);
        for(int i = 0; i < s->quoram_size - 1; i++) {
            if(s->last_entry) {
                s->next_index[i] = s->last_entry->index + 1;
            } else {
                s->next_index[i] = 1;
            }
            s->match_index[i] = 0;
        }
        send_heartbeat(s);
    }
}

static void election_timeout_callback(int fd, short event, void *arg) {
    struct server_context_t *s = (struct server_context_t *)arg;
    if(event & EV_TIMEOUT) {
        DBG_LOG(LOG_INFO, "[%s][%d] election timeout occurred", ss[s->state], s->current_term);
        //election timeout occurred -- do the candidate activities
        s->state = CANDIDATE;    
        s->current_leader = -1;
        s->voted_for = s->id;
        s->current_term++;
        save_state(s);

        s->current_votes = 1;

        //reset the previous election timer and add new one such that
        //if split vote occurs we have one more election
        reset_timers(s, true, false);

        //check if quoram_size already equal to required majoriy
        int votes_needed = s->quoram_size/2 + 1;
        if(votes_needed == s->current_votes) {
            s->state = LEADER;
            s->current_leader = s->id;
            save_state(s);
        } else {
            //send RequestVote RPC to pairs
            struct request_vote_input_t *input = (struct request_vote_input_t *)
                malloc(sizeof(struct request_vote_input_t));
            if(!input) {
                set_term(s, s->current_term - 1);
                return;
            }
            input->term = s->current_term;
            input->candidate_id = s->id;
            if(s->last_entry) {
                input->last_log_index = s->last_entry->index;
                input->last_log_term = s->last_entry->term;
            } else {
                input->last_log_index = 0; 
                input->last_log_term = 0;
            }

            struct method_t *rpc = make_request_vote_rpc_method(input);
            if(!rpc) {
                free(input);
                set_term(s, s->current_term - 1);
                return;
            }

            for(int i = 0; i < s->quoram_size - 1; i++) {
                if(rpc_call(s->rpc_c, s->peers[i]->dest, 
                            rpc, request_vote_cb, s)) {
                    //just log warning, as peers might go down
                    DBG_LOG(LOG_WARN, "[%s][%d] RPC call failed for peer %d", 
                            ss[s->state], s->current_term, s->peers[i]->id);
                }
            }
            free(input);
            free_method_t(rpc);
        } //end else-if
    }
}

static void heartbeat_timeout_callback(int fd, short event, void *arg) {
    struct server_context_t *s = (struct server_context_t *)arg;
    if(event & EV_TIMEOUT) {
        DBG_LOG(LOG_DEBUG, "[%s][%d] Heartbeat timeout occurred", ss[s->state], s->current_term);
        if(s->state == LEADER) {
            //heartbeat timout occured -- send empty append_entries
            send_heartbeat(s);
        }
    }
}

struct server_context_t *init_raft_server(int id, const char *basedir, 
    const char *host, int port, enum rpc_proto proto, 
    long election_timeout, long heartbeat_timeout) {
    struct server_context_t *s = (struct server_context_t *)malloc(
        sizeof(struct server_context_t));
    if(!s) {
        return NULL;
    }
    s->host = u_strdup(host);
    s->port = port;
    s->id = id;
    s->state = FOLLOWER;
    s->current_term = 0;
    s->commit_index = 0; 
    s->commit_term = 0; 
    s->log_last_applied = 0;
    s->quoram_size = 1; //TODO: read from config file
    s->current_votes = 0;

    s->basedir = u_strdup(basedir);
    char *logdir = path_join(basedir, RAFT_LOGDIR);
    s->log = init_log(logdir);
    if(!s->log) {
        deinit_raft_server(s);
        free(logdir);
        return NULL;
    }
    free(logdir);
    s->last_entry = get_last_entry(s->log);

    load_state(s);

    s->next_index = 0;
    s->match_index = 0;

    s->stm = init_stm(s);

    s->base = event_base_new();
    if(!s->base) {
        //LOG:FATAL event loop init failed
        deinit_raft_server(s);
        return NULL;
    }

    s->timer_el = event_new(s->base, -1, EV_PERSIST, election_timeout_callback, s);
    s->timer_hb = event_new(s->base, -1, EV_PERSIST, heartbeat_timeout_callback, s);

    s->election_timeout_ = election_timeout;
    s->election_timeout = time_in_timeval(election_timeout);
    s->heartbeat_timeout_ = heartbeat_timeout;
    s->heartbeat_timeout = time_in_timeval(heartbeat_timeout);
    if(!s->timer_el || !s->timer_hb || !s->election_timeout 
        || !s->heartbeat_timeout) {
        //LOG:FATAL timer creation failed
        deinit_raft_server(s);
        return NULL;
    }
    s->last_heartbeat_at = 0;

    //initially just add election timer and add heartbeat timer later
    //when this server elected as the leader
    if(event_add(s->timer_el, s->election_timeout)) {
        //LOG:FATAL timer event registration failed
        deinit_raft_server(s);
        return NULL;
    }

    s->rpc_s = init_rpc(s->base);
    s->rpc_c = init_rpc(s->base);
    if(!s->rpc_s || !s->rpc_c) {
        //LOG:FATAL RPC init failed
        deinit_raft_server(s);
        return NULL;
    }

    struct rpc_target *listen_dest = (struct rpc_target *)
        malloc(sizeof(struct rpc_target));
    if(!listen_dest) {
        deinit_raft_server(s);
        return NULL;
    }
    listen_dest->proto = HTTP;
    listen_dest->host = s->host;
    listen_dest->port = s->port;

    int res = 0;
    res |= init_rpc_server(s->rpc_s, listen_dest);
    res |= init_rpc_client(s->rpc_c);
    if(res) {
        //LOG:FATAL rpc server init failed
        deinit_raft_server(s);
        return NULL;
    }
    free(listen_dest);
    
    res = 0;
    res |= rpc_register(s->rpc_s, APPEND_ENTRIES_RPC, handle_append_entries, s);
    res |= rpc_register(s->rpc_s, REQUEST_VOTE_RPC, handle_request_vote, s);
    if(res) {
        //LOG:FATAL method registration failed
        deinit_raft_server(s);
        return NULL;
    }
    s->current_leader = -1;
    
    return s;
}

int add_pairs(struct server_context_t *s,  int npairs, int id[], const char *host[], int port[]) {
    s->peers = (struct peer_t **)malloc(sizeof(struct peer_t *)*npairs);
    if(!s->peers) {
        return 1;
    }

    for(int i = 0; i < npairs; i++) {
        s->peers[i] = (struct peer_t *)malloc(sizeof(struct peer_t));
        struct rpc_target *t = (struct rpc_target *)
            malloc(sizeof(struct rpc_target));
        char *u_host = u_strdup(host[i]);
        if(s->peers[i] && t && u_host) {
            t->host = u_host;
            t->port = port[i];
            s->peers[i]->id = id[i];
            s->peers[i]->dest = t;
        } else {
            if(s->peers[i]) free(s->peers[i]);
            if(t) free(t);
            if(u_host) free(u_host);
            for(int j = 0; j < i; j++) {
                free(s->peers[j]->dest->host);
                free(s->peers[j]->dest);
                free(s->peers[j]);
            }
            free(s->peers);

            s->peers = NULL;
            return 1;
        }
    }

    s->quoram_size = npairs + 1;
    if(s->quoram_size > 1) {
        //XXX: realloc could be used
        if(s->next_index) free(s->next_index);
        if(s->match_index) free(s->match_index);
        if(s->last_heartbeat_at) free(s->last_heartbeat_at);

        s->next_index = (uint64_t *)malloc(sizeof(uint64_t)*(npairs));
        s->match_index = (uint64_t *)malloc(sizeof(uint64_t)*(npairs));
        s->last_heartbeat_at = (struct timeval *)malloc(sizeof(struct timeval)*(npairs));
        if(!s->next_index || !s->match_index || !s->last_heartbeat_at) {
            deinit_raft_server(s);
            return 1;
        }
        struct timeval tv;
        evutil_gettimeofday(&tv, NULL);
        for(int i = 0; i < npairs; i++) {
            if(s->last_entry) {
                s->next_index[i] = s->last_entry->index + 1;
            } else {
                s->next_index[i] = 1;
            }
            s->match_index[i] = 0;
            s->last_heartbeat_at[i] = tv;
        }
    }

    return 0;
}

int start_server(struct server_context_t *s, int clientport) {
    DBG_LOG(LOG_INFO, "[%s][%d] starting client handler at port = %d", ss[s->state], s->current_term, clientport);

    start_client_handler(s, clientport);

    DBG_LOG(LOG_INFO, "[%s][%d] starting server's event loop", ss[s->state], s->current_term);

    return event_base_dispatch(s->base);
}

void deinit_raft_server(struct server_context_t *s) {
    if(s->peers){
        for(int i = 0; i < s->quoram_size; i++) {
            if(s->peers[i]) {
                if(s->peers[i]->dest) {
                    free(s->peers[i]->dest);
                }
                free(s->peers[i]);
            }
        }
        free(s->peers);
    }
    if(s->last_heartbeat_at) {
        free(s->last_heartbeat_at);
    }
    if(s->next_index) {
        free(s->next_index);
    }
    if(s->match_index) {
        free(s->match_index);
    }
    if(s->rpc_s) {
        deinit_rpc(s->rpc_s);
    }
    if(s->rpc_c) {
        deinit_rpc(s->rpc_c);
    }
    if(s->timer_el) {
        event_free(s->timer_el);
    }
    if(s->timer_hb) {
        event_free(s->timer_hb);
    }
    if(s->election_timeout) {
        free(s->election_timeout);
    }
    if(s->heartbeat_timeout) {
        free(s->heartbeat_timeout);
    }
    if(s->log) {
        deinit_log(s->log);
    }
    if(s->stm) {
        deinit_stm(s->stm);
    }

    if(s->base) {
        event_base_free(s->base);
    }
    if(s->host) {
        free(s->host);
    }
    if(s->basedir) {
        free(s->basedir);
    }
    free(s);
}

#ifdef TEST_RAFT
#include <stdlib.h>

int ports[] = {12320, 12321, 12322};

int main(int argc, char *argv[]) {
    if(argc < 2) {
        printf("Usage: raftd <id> <basedir>\n");
        return 1;
    }
    base64_init();

    int id = atoi(argv[1]);
    if(id < 1 || id > 3) {
        printf("Usage: raftd <id>\n");
        printf("provided <id> > 0 && <id> <= 3\n");
        return 1;
    }
    srandom(id);

    verbosity = LOG_DEBUG;

    struct server_context_t *s = init_raft_server(id, argv[2], 
            "0.0.0.0", ports[id - 1], HTTP, 1000000l, 500000l);
    int peers[2];
    int peer_ports[2];
    for(int i = 1, j = 0; i <= 3; i++) {
        if(id != i) {
            peers[j] = i; 
            peer_ports[j] = ports[i - 1];
            j++;
        }
    }
    printf("Server port = %d\n", ports[id - 1]);
    printf("Peer ports =  %d %d\n", peer_ports[0], peer_ports[1]);
    const char *hosts[] = { "0.0.0.0", "0.0.0.0" };
    if(s) {
        if(add_pairs(s, 2, peers, hosts, peer_ports)) {
            DBG_LOG(LOG_FATAL, "[%s][%d] Error while adding peers to server", ss[s->state], s->current_term);
            deinit_raft_server(s);
            return 1;
        } 
    } else {
        DBG_LOG(LOG_FATAL, "[?][?] Error while creating server instance");
        exit(1);
    }
    int clientport = ports[id - 1] + 10;
    printf("Client port = %d\n", clientport);
    start_server(s, clientport);
    
    deinit_raft_server(s);
    base64_cleanup();

    return 0;
}

#endif
