#include <jansson.h>
#if !defined(__APPLE__)
#include <malloc.h>
#else
#include <stdlib.h>
#endif
#include <string.h>

#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/keyvalq_struct.h>

#include "rpc.h"

const char *METHOD_JSON_KEY = "method";
const char *PARAMS_JSON_KEY = "params";

const char *RESULT_TYPE_JSON_KEY = "result-type";
const char *RESULT_JSON_KEY = "result";

static char *private_strdup(const char *str)
{
    char *new_str;
    size_t len;
    len = strlen(str);
    if(len == (size_t)-1)
        return NULL;
    new_str = malloc(len + 1);
    if(!new_str)
        return NULL;
    memmove(new_str, str, len + 1);
    return new_str;
}

int add_method_entry(struct method_lookup *lookup, const char *method,
    rpc_method_impl_t target_method_impl, void *data) {
    int i;
    for(i = 0; i < METHOD_LOOKUP_CACHE_SIZE; i++) {
        if(!lookup->cached_method_name[i]) {
            lookup->cached_method_name[i] = private_strdup(method);
            lookup->cached_target_method_impl[i] = target_method_impl;
            lookup->cached_satellite_data[i] = data;
            break;
        }
    }
    if(i == METHOD_LOOKUP_CACHE_SIZE) {
        struct method_entry *entry = (struct method_entry *)malloc(
            sizeof(struct method_entry));
        if(!entry) {
            return 1;
        }
        entry->method_name = private_strdup(method);
        entry->target_method_impl = target_method_impl;
        entry->satellite_data = data;
        entry->next = NULL;

        if(lookup->head == NULL) {
            lookup->head = entry;
        } else {
            struct method_entry *iter = lookup->head;
            while(iter->next != NULL) {
                iter = iter->next;
            }
            iter->next = entry;
        }
    }

    return 0;
}

rpc_method_impl_t search_method_entry(const struct method_lookup *lookup, const char *method, void **data) {
    for(int i = 0; i < METHOD_LOOKUP_CACHE_SIZE; i++) {
        if(lookup->cached_method_name[i] &&
            0 == strcmp(lookup->cached_method_name[i], method)) {
            *data = lookup->cached_satellite_data[i];
            return lookup->cached_target_method_impl[i];
        }
    }
    struct method_entry *iter = lookup->head;
    while(iter != NULL) {
        if(0 == strcmp(iter->method_name, method)) {
            *data = iter->satellite_data;
            return iter->target_method_impl;
        }
    }
    return NULL;
}


json_t *jsonize_param(const struct data_t *param) {
    switch(param->type) {
        case RPC_INT:
            return json_integer(*(json_int_t *)param->value);
        case RPC_REAL:
            return json_real(*(double *)param->value);
        case RPC_STRING:
            return json_string((char *)param->value);
        case RPC_VECTOR: {
            json_t *vector = json_array();
            if(vector) {
                for(int i = 0; i < param->length; i++) {
                    json_t *json_child = jsonize_param(param->child[i]);
                    if(!json_child || json_array_append_new(vector, json_child)) {
                        if(json_child) json_decref(json_child);
                        json_decref(vector);
                        return NULL;
                    }
                }
                return vector;
            }
        }
    }
    return NULL;
}

struct data_t *deserialize_data(json_t *json_data) {
    struct data_t *data = (struct data_t *)malloc(sizeof(struct data_t));
    if(json_is_integer(json_data)) {
        data->type = RPC_INT;
        data->value = (json_int_t *)malloc(sizeof(json_int_t));
        *(json_int_t *)data->value = json_integer_value(json_data);
    } else if(json_is_real(json_data)) {
        data->type = RPC_REAL;
        data->value = (double *)malloc(sizeof(double));
        *(double *)data->value = json_real_value(json_data);
    } else if(json_is_string(json_data)) {
        data->type = RPC_STRING;
        data->value = private_strdup(json_string_value(json_data));
    } else if(json_is_array(json_data)) {
        data->type = RPC_VECTOR;
        data->length = json_array_size(json_data);
        data->child = (struct data_t **)malloc(sizeof(struct data_t *)*data->length);
        for(int i = 0; i < data->length; i++) {
            data->child[i] = deserialize_data(json_array_get(json_data, i));
        }
    } else {
        return NULL;
    }
    return data;
}

// method call will be serialized in following json format
// {
//        "method" : "method-name",
//        "params" [
//            <serialization of param1>,
//            <serialization of param2>,
//            ...
//        ]
// }
char *serialize_method_call(const struct method_t *method) {
    char *data = NULL;

    json_t *root = json_object();
    json_t *json_method = json_string(method->name);
    json_t *json_params = json_array();

    if(!root || !json_method || !json_params) {
        goto error;
    }

    if(json_object_set(root, METHOD_JSON_KEY, json_method)) {
        goto error;
    }

    if(json_params) {
        for(int i = 0; i < method->nparams; i++) {
            json_t *json_param = jsonize_param(method->params[i]);
            if(!json_param || json_array_append_new(json_params, json_param)) {
                if(json_param) json_decref(json_param);
                goto error;
            }
        }
        if(json_object_set(root, PARAMS_JSON_KEY, json_params)) {
            goto error;
        }
        data = json_dumps(root, JSON_COMPACT | JSON_PRESERVE_ORDER);
    }

error:
    if(json_params) json_decref(json_params);
    if(json_method) json_decref(json_method);
    if(root) json_decref(root);

    return data;
}

// method result is expected in following format
// {
//        "result-type" : "<SUCCESS/ERROR>",
//        "result" : <error string or actual result>
// }
// return value or error needs to be freed
struct data_t *deserialize_result(const char *serialized, char **error) {
    json_t *root;
    json_error_t parse_error;
    
    root = json_loads(serialized, 0, &parse_error);

    if(!root || !json_is_object(root)) {
        if(root) json_decref(root);
        return NULL;
    }

    json_t *result_type = json_object_get(root, RESULT_TYPE_JSON_KEY);
    json_t *result = json_object_get(root, RESULT_JSON_KEY);

    if(!result_type || !result) {
        json_decref(root);
        return NULL;
    }

    if(!json_is_string(result_type)) {
        json_decref(root);
        return NULL;
    }

    if(0 == strcmp("SUCCESS", json_string_value(result_type))) {
        struct data_t *data = deserialize_data(result);
        json_decref(root);
        return data;
    } else {
        if(!json_is_string(result)) {
            json_decref(root);
            return NULL;
        }
        *error = private_strdup(json_string_value(result));
        json_decref(root);
    }

    return NULL;
}

//deserializes the method which serialized at caller
//returns method name to be called and the params/nparams
//are the arguments to the method call
//returns NULL on error
struct method_t *deserialize_method_call(const char *serialized) {
    json_t *root;
    json_error_t parse_error;
    
    root = json_loads(serialized, 0, &parse_error);

    if(!root || !json_is_object(root)) {
        if(root) json_decref(root);
        return NULL;
    }

    json_t *json_method_name = json_object_get(root, METHOD_JSON_KEY);
    json_t *json_method_params = json_object_get(root, PARAMS_JSON_KEY);

    if(!json_method_name || !json_method_params) {
        json_decref(root);
        return NULL;
    }
    
    char *method_name = NULL;
    if(!json_is_string(json_method_name)) {
        json_decref(root);
        return NULL;
    }
    method_name = private_strdup(json_string_value(json_method_name));

    struct data_t *envelope = deserialize_data(json_method_params);
    if(!envelope || envelope->type != RPC_VECTOR) {
        free(method_name);
        json_decref(root);
        return NULL;
    }

    struct method_t *mtd = (struct method_t *)
        malloc(sizeof(struct method_t));
    if(!mtd) {
        free(method_name);
        free_data_t(envelope);
        json_decref(root);
        return NULL;
    }

    mtd->name = method_name;
    mtd->params = envelope->child;
    mtd->nparams = envelope->length;

    free(envelope);
    json_decref(root);

    return mtd;
}

char *serialize_result(const struct data_t *result, const char *error) {
    char *data = NULL;

    json_t *root = json_object();
    if(!root) {
        return NULL;
    }
    
    json_t *json_result_type;
    json_t *json_result;
    if(error) {
        json_result_type = json_string("ERROR");
        json_result = json_string(error);
        if(!json_result_type || json_object_set(root, RESULT_TYPE_JSON_KEY, json_result_type) 
            || !json_result || json_object_set(root, RESULT_JSON_KEY, json_result)) {
            goto error;
        }
    } else {
        json_result_type = json_string("SUCCESS");
        json_result = jsonize_param(result);
        if(!json_result_type || json_object_set(root, RESULT_TYPE_JSON_KEY, json_result_type) 
            || !json_result || json_object_set(root, RESULT_JSON_KEY, json_result)) {
            goto error;
        }
    }
    data = json_dumps(root, JSON_COMPACT | JSON_PRESERVE_ORDER);

error:
    if(json_result) json_decref(json_result);
    if(json_result_type) json_decref(json_result_type);
    if(root) json_decref(root);
    
    return data;
}

void free_data_t(struct data_t *data) {
    if(data->type != RPC_VECTOR) {
        free(data->value);
        free(data);
    } else {
        for(int i = 0; i < data->length; i++) {
            free_data_t(data->child[i]);
        }
        free(data->child);
        free(data);
    }
}

void free_method_t(struct method_t *method) {
    for(int i = 0; i < method->nparams; i++) {
        free_data_t(method->params[i]);
    }
    free(method->params);
    free(method->name);
    free(method);
}

#define CONTENT_LENGTH_HEADER "Content-Length"
#define MAX_CONTENT_LENGTH (4096)
static char *read_req_buffer(struct evhttp_request *req) {
    struct evkeyvalq *headers = 
        evhttp_request_get_input_headers(req);
    int content_length = MAX_CONTENT_LENGTH;
    //TODO: use evhttp_find_header
    for(struct evkeyval *header = headers->tqh_first; header;
            header = header->next.tqe_next) {
        //TODO: should be strcmp ignoring case
        if(0 == strcmp(header->key, CONTENT_LENGTH_HEADER)) {
            content_length = atoi(header->value);
            break;
        }
    }
    char *str = (char *)malloc(content_length + 1);
    if(!str) {
        return NULL;
    }

    struct evbuffer *buf = evhttp_request_get_input_buffer(req);
    int nread = 0;
    while (evbuffer_get_length(buf)) {
        char cbuf[128];
        int n = evbuffer_remove(buf, cbuf, sizeof(cbuf));
        if(n > 0) {
            memmove(str + nread, cbuf, n);
            nread += n;
        }
    }
    str[nread] = '\0';

    return str;
}

static void server_rpc_cb(struct evhttp_request *req, void *arg) {
    struct rpc_context *rpc_context = (struct rpc_context *)arg;

    char *json = read_req_buffer(req);
    if(!json) {
        evhttp_send_error(req, 500, "Internal server error");
        return;
    }

    struct method_t *method = deserialize_method_call(json);
    if(!method) {
        evhttp_send_error(req, 500, "Error deserializing method call");
        free(json);
        return;
    }
    void *satellite_data;
    rpc_method_impl_t impl = search_method_entry(rpc_context->lookup, method->name, &satellite_data);
    if(!impl) {
        evhttp_send_error(req, 500, "Method not found");
    } else {
        //impl should also throw error
        char *err;
        struct data_t *result = impl(method->params, method->nparams, &err, satellite_data);
        char *json_result = NULL;

        struct evbuffer *evb = evbuffer_new();

        if(result && (json_result = serialize_result(result, NULL)) && evb) {
            if(evbuffer_add(evb, json_result, strlen(json_result))) {
                evhttp_send_error(req, 500, "Internal server error");
            } else {
                evhttp_send_reply(req, 200, "OK", evb);
            }
        } else {
            if(err) {
                evhttp_send_error(req, 500, err);
                free(err);
            } else {
                evhttp_send_error(req, 500, "Internal server error");
            }
        }

        if(evb) evbuffer_free(evb);
        if(json_result) free(json_result);
        if(result) free_data_t(result);
    }

    free(json);
    free_method_t(method);
}

struct rpc_context *init_rpc(struct event_base *base) {
    struct rpc_context *cntxt = 
        (struct rpc_context *)malloc(sizeof(struct rpc_context));
    if(!cntxt) {
        return NULL;
    }
    cntxt->base = base;
    cntxt->lookup = (struct method_lookup *)malloc(sizeof(struct method_lookup));
    if(!cntxt->lookup) {
        free(cntxt);
        return NULL;
    }
    for(int i = 0; i < METHOD_LOOKUP_CACHE_SIZE; i++) {
        cntxt->lookup->cached_method_name[i] = NULL;
    }
    cntxt->lookup->head = NULL;
    return cntxt;
}

void deinit_rpc(struct rpc_context *context) {
    if(context->http) {
        evhttp_free(context->http);
    }
    if(context->lookup) {
         struct method_entry *iter = context->lookup->head;
         while(iter) {
            struct method_entry *tmp = iter;
            iter = iter->next;
            free(tmp->method_name);
            free(tmp);
         }
         for(int i = 0; i < METHOD_LOOKUP_CACHE_SIZE; i++) {
            char *m = context->lookup->cached_method_name[i];
            if(m) free(m);
         }
         free(context->lookup);
    }
    free(context);
}

int init_rpc_server(struct rpc_context *rpc_context, const struct rpc_target *dest) {
    //should check dest->proto and do http handling only when proto=HTTP
    rpc_context->http = evhttp_new(rpc_context->base);
    if(!rpc_context->http) {
        return 1;
    }
    evhttp_set_cb(rpc_context->http, "/rpc", server_rpc_cb, rpc_context);

    rpc_context->handle = evhttp_bind_socket_with_handle(
        rpc_context->http, dest->host, dest->port);
    if(!rpc_context->handle) {
        evhttp_free(rpc_context->http);
        return 1;
    }

    return 0;
}

int init_rpc_client(struct rpc_context *rpc_context) {
    rpc_context->http = NULL;
    return 0;
}

struct client_rpc_callback_with_data {
    rpc_callback cb;
    void *data;
    struct evhttp_connection *evcon; 
};


static void http_request_done(struct evhttp_request *req, void *ctx) {
    struct client_rpc_callback_with_data *m_ctx = 
        (struct client_rpc_callback_with_data *)ctx;
    rpc_callback client_cb = m_ctx->cb;
    void *data = m_ctx->data;
    struct evhttp_connection *evcon = m_ctx->evcon;

    free(m_ctx);
    if(req == NULL) {
        client_cb(NULL, "(req=NULL) Unknown error occurred while remote procedure", data);
        return;
    }
    int res_code = evhttp_request_get_response_code(req);
    char *res_code_line = "Internal server error";
    printf("Got response for req %p with ctx = %p res_code = %d\n", 
            req, ctx, res_code);
    if(res_code == 500) {
        client_cb(NULL, res_code_line, data);
    } else if(res_code != HTTP_OK) {
        if(res_code == 0) {
            client_cb(NULL, "host not reachable", data);
        } else {
            client_cb(NULL, "communication error", data);
        }
    } else {
        char *json = read_req_buffer(req);
        if(!json) {
            client_cb(NULL, "(json=NULL) Unknown error occurred while remote procedure", data);
            return;
        }

        char *err = NULL;
        struct data_t *result = deserialize_result(json, &err);

        client_cb(result, err, data);

        if(result) {
            free_data_t(result);
        }
        if(err) {
            free(err);
        }
        if(json) {
            free(json);
        }
    }
    if(evcon) {
        evhttp_connection_free(evcon);
    }
}

int rpc_call(struct rpc_context *context, 
    const struct rpc_target *dest, const struct method_t *method, 
    rpc_callback client_cb, void *data) {

    //TODO: check the protocol in dest->proto and do http
    // request only if dest->proto == HTTP
    int res = 1;
    struct evhttp_connection *evcon = NULL;
    struct evhttp_request *req = NULL;
    char *json_method = NULL;
    struct client_rpc_callback_with_data *ctx = NULL;

    //TODO: can be make http_connection as part of peer_t?
    evcon = evhttp_connection_base_new(
        context->base, NULL, dest->host, dest->port);
    if (!evcon) {
        goto cleanup;
    }
    ctx = (struct client_rpc_callback_with_data *)malloc(
        sizeof(struct client_rpc_callback_with_data));
    if(!ctx) {
        goto cleanup;
    }
    ctx->cb = client_cb;
    ctx->data = data;
    ctx->evcon = evcon;

    req = evhttp_request_new(http_request_done, ctx);
    if (!req) {
        goto cleanup;
    }

    char uri[256]; 
    snprintf(uri, sizeof(uri)-1, "http://%s:%d/rpc", dest->host, dest->port);
    
    json_method  = serialize_method_call(method);
    if(!json_method) {
        goto cleanup;
    }

    struct evbuffer *output_buffer = evhttp_request_get_output_buffer(req);
    evbuffer_add(output_buffer, json_method, strlen(json_method));

    char content_length[20];
    snprintf(content_length, sizeof(content_length)-1, "%d", (int)strlen(json_method));

    struct evkeyvalq *output_headers = evhttp_request_get_output_headers(req);
    evhttp_add_header(output_headers, "Host", dest->host);
    evhttp_add_header(output_headers, "Connection", "close");
    evhttp_add_header(output_headers, "Content-Length", content_length);

    printf("Sending req %p with ctx = %p\n", req, ctx);
    int r = evhttp_make_request(evcon, req, EVHTTP_REQ_POST, uri);
    if(!r) {
        res = 0;
    }

cleanup:
    if(json_method)
        free(json_method);
    if(res && ctx)
        free(ctx);
    if(res && evcon)
        evhttp_connection_free(evcon);
    if(res && req)
        evhttp_request_free(req);

    return res;
}

int rpc_register(struct rpc_context *context, const char *method_name,
    rpc_method_impl_t target_method_impl, void *data) {
    void *ignore;
    if(search_method_entry(context->lookup, method_name, &data)) {
        //method already registered (polymorphic methods are not supported)
        return 1;
    }
    if(add_method_entry(context->lookup, method_name, target_method_impl, data)) {
        return 1;
    }
    return 0;
}


#ifdef TEST_RPC_SERVER

struct data_t *test1(struct data_t *params[], int nparams, char **err, void *data) {
    int *inc = (int *)data;
    int in = *(json_int_t *)params[0]->value;
    struct data_t *r = (struct data_t *)malloc(sizeof(struct data_t));
    json_int_t *ri = (json_int_t *)malloc(sizeof(json_int_t));
    *ri = in + *inc;
    r->type = RPC_INT;
    r->value = ri;
    return r;
}

#include <signal.h>
struct rpc_context *c;

void exit_handler(int int_num) {
    if(c) {
        deinit_rpc(c);
    }
}

int main() {
    struct event_base *base = event_base_new();
    c = init_rpc(base);
    int inc = 2;
    rpc_register(c, "test1", test1, &inc);
    struct rpc_target dest = { HTTP, "0.0.0.0", 12321 };
    init_rpc_server(c, &dest);

    signal(SIGINT, exit_handler);

    event_base_dispatch(base);

    return 0;
}

#endif

#ifdef TEST_RPC_CLIENT

void test1_cb(struct data_t *result, char *err, void *data) {
    if(err) {
        printf("error = %s\n", err);
    } else {
        int *i = (int *)data;
        if(i && result && *i == 42) {
            printf("success = %d\n", *(json_int_t *)result->value);
        } else {
            printf("error = ctx or result is invalid\n");
        }
    }
}

int main() {
    struct event_base *base = event_base_new();
    struct rpc_target dest = { HTTP, "127.0.0.1", 12321 };

    struct rpc_context *c = init_rpc(base);
    init_rpc_client(c);

    json_int_t i = 40;
    struct data_t param1 = { RPC_INT, &i, NULL, 0};
    const struct data_t* param[] = { &param1 };
    struct method_t m = {"test1", (struct data_t**)param, 1};
    int j = 42;
    rpc_call(c, &dest, &m, test1_cb, &j);

    event_base_dispatch(base);

    deinit_rpc(c);
    event_base_free(base);

    return 0;
}

#endif

#ifdef TEST_RPC_JSON

int main() {
    {
        struct method_t m = {"test0", NULL, 0};
        char *ser = serialize_method_call(&m);
        char *expected = "{\"method\":\"test0\",\"params\":[]}";
        if(strcmp(ser, expected))  {
            printf("failed = actual:%s expected:%s\n", ser, expected);
        }
        free(ser);
    }
    json_int_t i = 1;
    struct data_t param1 = { RPC_INT, &i, NULL, 0};
    {
        const struct data_t* param[] = { &param1 };
        struct method_t m = {"test1", (struct data_t**)param, 1};
        char *ser = serialize_method_call(&m);
        char *expected = "{\"method\":\"test1\",\"params\":[1]}";
        if(strcmp(ser, expected))  {
            printf("failed = actual:%s expected:%s\n", ser, expected);
        }
        free(ser);
    }
    struct data_t param2 = { RPC_STRING, "searaft", NULL, 0};
    {
        const struct data_t* param[] = { &param1, &param2 };
        struct method_t m = {"test2",(struct data_t**)param, 2};
        char *ser = serialize_method_call(&m);
        char *expected = "{\"method\":\"test2\",\"params\":[1,\"searaft\"]}";
        if(strcmp(ser, expected))  {
            printf("failed = actual:%s expected:%s\n", ser, expected);
        }
        free(ser);
    }
    double d = 12.01;
    struct data_t param3 = { RPC_REAL, &d, NULL, 0};
    {
        struct data_t *param4[] = { &param1, &param2 };
        const struct data_t param5 = { RPC_VECTOR, NULL, param4, 2};
        const struct data_t* param[] = { &param3, &param5 };
        struct method_t m = {"test3",(struct data_t**)param, 2};
        char *ser = serialize_method_call(&m);
        char *expected = "{\"method\":\"test3\",\"params\":[12.01,[1,\"searaft\"]]}";
        if(strcmp(ser, expected))  {
            printf("failed = actual:%s expected:%s\n", ser, expected);
        }
        free(ser);
    }
    {
        char *error = NULL;
        struct data_t *data1 = deserialize_result("{\"result-type\": \"SUCCESS\", \"result\" : 1 }", &error);
        if(data1) {
            int val = *(json_int_t *)data1->value;
            if(val != 1) {
                printf("failed = actual:%d expected: 1\n", val);
            }
            free_data_t(data1);
        } else {
            printf("failed = actual: NULL expected: 1");
        }
    }
    {
        char *error = NULL;
        struct data_t *data1 = deserialize_result("{\"result-type\": \"SUCCESS\", \"result\" : [1, \"searaft\"]  }", &error);
        if(data1) {
            if(data1->type == RPC_VECTOR) {
                if(data1->length == 2) {
                    if(data1->child[0]->type != RPC_INT || data1->child[1]->type != RPC_STRING
                        || *(json_int_t *)data1->child[0]->value != 1 
                        ||  strcmp((char *)data1->child[1]->value, "searaft")) {
                        printf("failed = object value dont match\n");
                    }
                } else {
                    printf("failed = actual: %d expected: 2\n", data1->length);
                }
            } else {
                printf("failed = actual: %d expected: %d\n", data1->type, RPC_VECTOR);
            }
            free_data_t(data1);
        } else {
            printf("failed = actual: NULL expected: 1");
        }
    }
    {
        char *error = NULL;
        (void)deserialize_result("{\"result-type\": \"ERROR\", \"result\" : \"test error\"}", &error);
        if(error) {
            if(strcmp("test error", error)) {
                printf("failed = actual:%s expected: \"test error\"\n", error);
            }
            free(error);
        } else {
            printf("failed = actual: NULL expected: \"test error\"");
        }
    }
    {
        struct method_t *mtd = deserialize_method_call("{\"method\" : \"test\", \"params\" : [1, 2]}");
        if(mtd && 0 == strcmp(mtd->name, "test")) {
            if(mtd->nparams == 2) {
                if(mtd->params[0]->type != RPC_INT || mtd->params[1]->type != RPC_INT
                    || *(json_int_t *)mtd->params[0]->value != 1 
                    || *(json_int_t *)mtd->params[1]->value != 2) {
                    printf("failed = param value dont match\n");
                } 
            } else {
                printf("failed = actual:%d expected: 2\n", mtd->nparams);
            }    
        } else {
            printf("failed = actual:%s expected: \"test\"\n", mtd->name);
        }
    }
    {
        json_int_t i = 42;
        struct data_t data1 = { RPC_INT, &i, NULL, 0};
        char *ser = serialize_result(&data1, NULL);
        char *expected = "{\"result-type\":\"SUCCESS\",\"result\":42}";
        if(strcmp(ser, expected))  {
            printf("failed = actual:%s expected:%s\n", ser, expected);
        }
        free(ser);
    }
    {
        char *ser = serialize_result(NULL, "test error");
        char *expected = "{\"result-type\":\"ERROR\",\"result\":\"test error\"}";
        if(strcmp(ser, expected))  {
            printf("failed = actual:%s expected:%s\n", ser, expected);
        }
        free(ser);
    }
    
    return 0;
}

#endif
