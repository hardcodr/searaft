
#ifndef SEARAFT_UTILS
#define SEARAFT_UTILS

#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

#define MAX_DBG_LOG_MSG 1024
#define DBG_LOG(...) log_print(__FILE__, __LINE__, __VA_ARGS__ )

enum LOG_SEVERITY {
    LOG_DEBUG,
    LOG_INFO,
    LOG_WARN,
    LOG_ERROR,
    LOG_FATAL
};

extern int verbosity;
extern char *dbg_logfile;
void log_print(const char *filename, const int lineno, 
        int level, const char *fmt, ...);

//base 64 encoding utility functions
void base64_init();

void base64_cleanup();

char *base64_encode(const unsigned char *data,
        size_t input_length, size_t *output_length);

unsigned char *base64_decode(const char *data,
        size_t input_length, size_t *output_length);

char *u_strdup(const char *str);

unsigned long time_in_micros(struct timeval *tv);

struct timeval *time_in_timeval(unsigned long micros);

//joins two paths and returns newly allocated string 
//appends the fileseparator at the of parent if none exists
//returns null of cannot alocate string
char *path_join(const char *parent, const char *child);

//sets exits = 1 if file exists or exist = 0 if file does
//not exists; method retruns 1 on success and 0 on failure 
int file_exists(const char *filename, int *exists);


//reads sizeof(int)-bytes from the file descriptor 
//(whose cursor thus moves ahead by 4-byte) and 
//stores it in the value.
//returns 1 if successful and 0 if not
int read_uint32(int fd, uint32_t *value);

//writes sizeof(int)-bytes to the file descriptor
//returns 1 if successful and 0 if not
int write_uint32(int fd, uint32_t value);

//reads sizeof(long)-bytes from the file descriptor 
//(whose cursor thus moves ahead by 8-byte) and 
//stores it in the value.
//returns 1 if successful and 0 if not
int read_uint64(int fd, uint64_t *value);

//writes sizeof(long)-bytes to the file descriptor
//returns 1 if successful and 0 if not
int write_uint64(int fd, uint64_t value);

#endif
