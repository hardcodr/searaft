#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "utils.h"

int verbosity = LOG_INFO;
char *dbg_logfile = "";

void log_print(const char *filename, const int lineno, int level, const char *fmt, ...) {
    va_list ap;
    char msg[MAX_DBG_LOG_MSG];

    if ((level&0xff) < verbosity) return;

    va_start(ap, fmt);
    vsnprintf(msg, sizeof(msg), fmt, ap);
    va_end(ap);

    static const int syslogLevelMap[] = { LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_FATAL };
    static const char *c = ".-*#@";
    FILE *fp;
    char buf[64];

    int log_to_stdout = dbg_logfile[0] == '\0';
    fp = log_to_stdout ? stdout : fopen(dbg_logfile,"a");
    if (!fp) return;

    int off;
    struct timeval tv;
    int role_char;
    pid_t pid = getpid();

    gettimeofday(&tv,NULL);
    off = strftime(buf,sizeof(buf),"%d %b %H:%M:%S.",localtime(&tv.tv_sec));
    snprintf(buf+off,sizeof(buf)-off,"%03d",(int)tv.tv_usec/1000);

    fprintf(fp,"%d: %s [%s:%d] %c %s\n",
            (int)getpid(), buf, filename, lineno, c[level], msg);

    fflush(fp);
    if(!log_to_stdout) fclose(fp);
}

//copied from stackoverflow

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

char *base64_encode(const unsigned char *data,
        size_t input_length,
        size_t *output_length) {

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}


unsigned char *base64_decode(const char *data,
        size_t input_length,
        size_t *output_length) {

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
            + (sextet_b << 2 * 6)
            + (sextet_c << 1 * 6)
            + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}


void base64_init() {
    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}


void base64_cleanup() {
    free(decoding_table);
}

char *u_strdup(const char *str) {
    char *new_str;
    size_t len;
    len = strlen(str);
    if(len == (size_t)-1)
        return NULL;
    new_str = malloc(len + 1);
    if(!new_str)
        return NULL;
    memmove(new_str, str, len + 1);
    return new_str;
}

unsigned long time_in_micros(struct timeval *tv) {
    return (1000000 * tv->tv_sec + tv->tv_usec);
}

struct timeval *time_in_timeval(unsigned long micros) {
    struct timeval *tv = (struct timeval *)malloc(
        sizeof(struct timeval));
    if(!tv) {
        return NULL;
    }

    tv->tv_sec = micros / 1000000;
    tv->tv_usec = micros % 1000000;

    return tv;
}

char *path_join(const char *parent, const char *child) {
    char *joinpath;
    int plen = strlen(parent);
    int clen = strlen(child);

    if(plen == 0 || parent[plen-1] == '/') {
        int pathlen = (plen + clen + 1);
        joinpath = (char *)malloc(pathlen);
        if(joinpath) {
            snprintf(joinpath, pathlen, "%s%s", parent, child);
        }
    } else {
        int pathlen = (plen + 1 + clen + 1);
        joinpath = (char *)malloc(pathlen);
        if(joinpath) {
            snprintf(joinpath, pathlen, "%s/%s", parent, child);
        }
    }
    return joinpath;
}

int file_exists(const char *filename, int *exists) {
    struct stat f_stat;
    
    int reply = stat(filename, &f_stat);
    if(reply == 0) {
        *exists = 1;
    } else if(errno == ENOENT) {
        *exists = 0;
    } else {
        perror("file_exists: ");
        return 0;
    }
    return 1;
}

int read_uint32(int fd, uint32_t *value) {
    union {
        unsigned char b[sizeof(uint32_t)];
        uint32_t i;
    } b2i;

    int nread = read(fd, b2i.b, sizeof(uint32_t));
    if(nread != sizeof(uint32_t)) {
        return 0;
    }
    *value = b2i.i;

    return 1;
}

int write_uint32(int fd, uint32_t value) {
    union {
        uint32_t i;
        unsigned char b[sizeof(uint32_t)];
    } i2b;

    i2b.i = value;
    int nwrite = write(fd, i2b.b, sizeof(uint32_t));
    if(nwrite != sizeof(uint32_t)) {
        DBG_LOG(LOG_WARN, "write %d bytes only", nwrite);
        return 0;
    }
    return 1;
}

int read_uint64(int fd, uint64_t *value) {
    union {
        unsigned char b[sizeof(uint64_t)];
        uint64_t i;
    } b2i;

    int nread = read(fd, b2i.b, sizeof(uint64_t));
    if(nread != sizeof(uint64_t)) {
        return 0;
    }
    *value = b2i.i;

    return 1;
}

int write_uint64(int fd, uint64_t value) {
    union {
        uint64_t i;
        unsigned char b[sizeof(uint64_t)];
    } i2b;

    i2b.i = value;
    int nwrite = write(fd, i2b.b, sizeof(uint64_t));
    if(nwrite != sizeof(uint64_t)) {
        DBG_LOG(LOG_WARN, "write %d bytes only", nwrite);
        return 0;
    }
    return 1;
}

#ifdef TEST_ENCODE_DECODE

#include <stdio.h>
#include <string.h>

int main() {
    unsigned char somedata[10] = { 0, 12, 12, 14, 56, 112, 45, 55, 111, 200 };
    base64_init();
    
    size_t outlen1 = 0;
    char *encoded = base64_encode(somedata, 10, &outlen1);
    char *str = (char *)malloc(outlen1+1);
    memcpy(str, encoded, outlen1);
    str[outlen1] = '\0';
    printf("success = encoded %s\n", str);

    size_t outlen2 = 0;
    unsigned char *decoded = base64_decode(encoded, outlen1, &outlen2);
    if(outlen2 == 10) {
        for(int i = 0; i < 10; i++) {
            if(somedata[i] != decoded[i]) {
                printf("failed = %d the character differ expected = %d actual = %d\n",
                    i, (int)somedata[i], (int)decoded[i]);
            }
        }
    } else {
        printf("failed = size doesnt match\n");
    }

    free(str);
    free(encoded);
    free(decoded);

    base64_cleanup();
    return 0;
}
#endif

#ifdef TEST_TIME_OF_DAY
#include <stdio.h>

int main() {
    struct timeval *tv = time_in_timeval(10222);
    if(tv) {
       printf("%d %d\n", tv->tv_sec, tv->tv_usec); 
       gettimeofday(tv, NULL);
       printf("%lu\n", time_in_micros(tv));
    }
}
#endif
