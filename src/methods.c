#include <string.h>
#if !defined(__APPLE__)
#include <malloc.h>
#else
#include <stdlib.h>
#endif

#include "methods.h"
#include "utils.h"

static char *private_strdup(const char *str)
{
    char *new_str;
    size_t len;
    len = strlen(str);
    if(len == (size_t)-1)
        return NULL;
    new_str = malloc(len + 1);
    if(!new_str)
        return NULL;
    memmove(new_str, str, len + 1);
    return new_str;
}

static struct data_t *uint64_to_data_t(int64_t value) {
    struct data_t *r = (struct data_t *)malloc(sizeof(struct data_t));
    if(!r) {
        return NULL;
    }

    r->type = RPC_INT;
    //TODO: remove reference to json_int_t
    // rpc.h should abstract away this
    r->value = (json_int_t *)malloc(sizeof(json_int_t));
    if(!r->value) {
        free(r);
        return NULL;
    }
    *(json_int_t *)r->value = value;

    return r;
}

static struct data_t *uint32_to_data_t(uint32_t value) {
    return uint64_to_data_t(value);
}

static struct data_t *str_to_data_t(const unsigned char *value, size_t len) {
    struct data_t *r = (struct data_t *)malloc(sizeof(struct data_t));
    if(!r) {
        return NULL;
    }

    size_t outlen = 0;
    char *encoded = base64_encode(value, len, &outlen);
    if(!encoded) {
        free(r);
        return NULL;
    }
    char *encoded_str = (char *)malloc(outlen + 1);
    if(!encoded_str) {
        free(r);
        free(encoded);
        return NULL;
    }
    memmove(encoded_str, encoded, outlen);
    encoded_str[outlen] = '\0';

    free(encoded);

    r->type = RPC_STRING;
    r->value = encoded_str;

    return r;
}

#define LOG_ENTRY_DATA_LEN (4)

static struct data_t *log_to_data_t(struct log_entry_t *e) {
    struct data_t *r = (struct data_t *)malloc(sizeof(struct data_t));
    struct data_t **child = (struct data_t **)malloc(
        sizeof(struct data_t *)*LOG_ENTRY_DATA_LEN);

    if(!r || !child) {
        if(r) free(r);
        if(child) free(child);
        return NULL;
    }

    r->type = RPC_VECTOR;
    r->child = child;
    r->length = LOG_ENTRY_DATA_LEN;

    child[0] = str_to_data_t(e->buffer, e->bufsize);
    child[1] = uint64_to_data_t(e->term);
    child[2] = uint64_to_data_t(e->index);
    child[3] = uint64_to_data_t(e->req_id);

    for(int i = 0; i < LOG_ENTRY_DATA_LEN; i++) {
        if(!child[i]) {
            r->length = i;
            free_data_t(r);
            return NULL;
        }
    }

    return r;
}



#define REQUEST_VOTE_PARAM_COUNT 4
struct method_t *make_request_vote_rpc_method(struct request_vote_input_t *input) {
    struct data_t **params = (struct data_t **)malloc(
        sizeof(struct data_t *)*REQUEST_VOTE_PARAM_COUNT);
    if(!params) {
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }

    params[0] = uint64_to_data_t(input->term);
    params[1] = uint32_to_data_t(input->candidate_id);
    params[2] = uint64_to_data_t(input->last_log_index);
    params[3] = uint64_to_data_t(input->last_log_term);

    char *method_name = private_strdup(REQUEST_VOTE_RPC);

    struct method_t *request_vote = 
        (struct method_t *)malloc(sizeof(struct method_t));
    if(!request_vote) {
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        if(method_name) free(method_name);
        for(int i = 0; i < REQUEST_VOTE_PARAM_COUNT; i++) {
            if(params[i]) {
                free_data_t(params[i]);
            }
        }
        free(params);
        return NULL;
    }

    request_vote->name = method_name;
    request_vote->params = params;
    request_vote->nparams = REQUEST_VOTE_PARAM_COUNT;

    return request_vote;
}

#define REQUEST_VOTE_RES_COUNT 2
struct data_t *make_request_vote_rpc_response(struct request_vote_output_t *output) {
    struct data_t *res = (struct data_t *)malloc(sizeof(struct data_t));
    if(!res){
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }

    res->type = RPC_VECTOR;
    res->length = REQUEST_VOTE_RES_COUNT;

    res->child = (struct data_t **)malloc(
        sizeof(struct data_t *)*REQUEST_VOTE_RES_COUNT);
    if(!res->child) {
        free(res); 
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }

    res->child[0] = uint64_to_data_t(output->term);
    res->child[1] = uint32_to_data_t(output->vote_granted);
    for(int i = 0; i < REQUEST_VOTE_RES_COUNT; i++) {
        if(!res->child[i]) {
            res->length = i;
            free_data_t(res);
            DBG_LOG(LOG_FATAL, "memory allocation failed");
            return NULL;
        }
    }

    return res;
}

#define APPEND_ENTRIES_PARAM_COUNT 6
struct method_t *make_append_entries_rpc_method(struct append_entries_input_t *input) {
    struct data_t **params = (struct data_t **)malloc(sizeof(struct data_t *)*APPEND_ENTRIES_PARAM_COUNT);
    if(!params) {
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }

    params[0] = uint64_to_data_t(input->term);
    params[1] = uint32_to_data_t(input->leader_id);
    params[2] = uint64_to_data_t(input->prev_log_index);
    params[3] = uint64_to_data_t(input->prev_log_term);
    params[4] = uint64_to_data_t(input->leader_commit_index);

    struct data_t *entries_array = (struct data_t *)malloc(sizeof(struct data_t));
    if(entries_array) {
        entries_array->type = RPC_VECTOR;
        entries_array->length = input->nentries;
        entries_array->child = (struct data_t **)malloc(
                sizeof(struct data_t *)*input->nentries);
        if(entries_array->child) {
            int alldone = 1;
            for(int i = 0; i < input->nentries; i++) {
                entries_array->child[i] = log_to_data_t(input->entries[i]);
                if(!entries_array->child[i]) {
                    entries_array->length = i;
                    alldone = 0; break;
                }
            }
            if(alldone) {
                params[5] = entries_array;
            } else {
                free_data_t(entries_array);
            }
        } else {
            free(entries_array);
        }
    }
    
    char *method_name = private_strdup(APPEND_ENTRIES_RPC);

    //none of the above allocation failure are handled assuming if 
    //one fails the subsequent allocation will also fail including 
    //allocation of append_entries hence all of them handled together 
    // below
    struct method_t *append_entries = (struct method_t *)malloc(
        sizeof(struct method_t));    
    if(!append_entries) {
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        if(method_name) free(method_name);
        for(int i = 0; i < APPEND_ENTRIES_PARAM_COUNT; i++) {
            if(params[i]) {
                free_data_t(params[i]);
            }
        }
        free(params);
        return NULL;
    }

    append_entries->name = method_name;
    append_entries->params = params;
    append_entries->nparams = APPEND_ENTRIES_PARAM_COUNT;

    return append_entries;
}

#define APPEND_ENTRIES_RES_COUNT 2
struct data_t *make_append_entries_rpc_response(struct append_entries_output_t *output) {
    struct data_t *res = (struct data_t *)malloc(sizeof(struct data_t));
    if(!res){
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }

    res->type = RPC_VECTOR;
    res->length = APPEND_ENTRIES_RES_COUNT;

    res->child = (struct data_t **)malloc(
        sizeof(struct data_t *)*APPEND_ENTRIES_RES_COUNT);
    if(!res->child) {
        free(res);
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }

    res->child[0] = uint64_to_data_t(output->term);
    res->child[1] = uint32_to_data_t(output->success);
    for(int i = 0; i < REQUEST_VOTE_RES_COUNT; i++) {
        if(!res->child[i]) {
            res->length = i;
            free_data_t(res);
            DBG_LOG(LOG_FATAL, "memory allocation failed");
            return NULL;
        }
    }

    return res;
}

struct request_vote_input_t *get_request_vote_input_params(struct data_t *params[], int nparams) {
    struct request_vote_input_t *r = (struct request_vote_input_t *)malloc(sizeof(struct request_vote_input_t));
    if(!r) {
        return NULL;
    }

    if(nparams != REQUEST_VOTE_PARAM_COUNT) {
        free(r);
        DBG_LOG(LOG_ERROR, "incorrect number of parameter in RPC, expected = %d, got = %d",
                REQUEST_VOTE_PARAM_COUNT, nparams);
        return NULL;
    }

    for(int i = 0; i < REQUEST_VOTE_PARAM_COUNT; i++) {
        if(!params[i]
            || params[i]->type != RPC_INT
            || !params[i]->value) {
            free(r);
            if(params[i]) {
                DBG_LOG(LOG_ERROR, "incorrect value in %dth param in RPC, expected = %d, got = %d, value = %x",
                        (i+1), RPC_INT, params[i]->type, params[i]->value);
            }
            return NULL;
        }
    }

    r->term = *(json_int_t *)params[0]->value;
    r->candidate_id = *(json_int_t *)params[1]->value;
    r->last_log_index = *(json_int_t *)params[2]->value;
    r->last_log_term = *(json_int_t *)params[3]->value;

    return r;
}

struct request_vote_output_t *get_request_vote_output(struct data_t *request_vote_response) {
    struct request_vote_output_t *r = (struct request_vote_output_t *)malloc(
        sizeof(struct request_vote_output_t));
    if(!r) {
        return NULL;
    }

    if(request_vote_response->length != REQUEST_VOTE_RES_COUNT) {
        free(r); 
        DBG_LOG(LOG_ERROR, "incorrect number of parameter in RPC response, expected = %d, got = %d",
                REQUEST_VOTE_RES_COUNT, request_vote_response->length);
        return NULL;
    }

    for(int i = 0; i < REQUEST_VOTE_RES_COUNT; i++) {
        if(!request_vote_response->child[i]
            || request_vote_response->child[i]->type != RPC_INT
            || !request_vote_response->child[i]->value) {
            free(r); 
            if(request_vote_response->child[i]) {
                DBG_LOG(LOG_ERROR, "incorrect value in %dth param in RPC, expected = %d, got = %d",
                        (i+1), RPC_INT, request_vote_response->child[i]->type);
            }
            return NULL;
        }
    }

    r->term = *(json_int_t *)request_vote_response->child[0]->value;
    r->vote_granted = *(json_int_t *)request_vote_response->child[1]->value;

    return r;

}

struct append_entries_input_t *get_append_entries_input_params(struct data_t *params[], int nparams) {
    struct append_entries_input_t *r = (struct append_entries_input_t *)malloc(
        sizeof(struct append_entries_input_t));
    if(!r) {
        return NULL;
    }

    if(nparams != APPEND_ENTRIES_PARAM_COUNT) {
        free(r); 
        DBG_LOG(LOG_ERROR, "incorrect number of parameter in RPC, expected = %d, got = %d",
                APPEND_ENTRIES_PARAM_COUNT, nparams);
        return NULL;
    }

    for(int i = 0; i < APPEND_ENTRIES_PARAM_COUNT - 1; i++) {
        if(!params[i]
                || params[i]->type != RPC_INT
                || !params[i]->value) {
            free(r);
            if(params[i]) {
                DBG_LOG(LOG_ERROR, "incorrect value in %dth param in RPC, expected = %d, got = %d",
                        (i+1), RPC_INT, params[i]->type);
            } else {
                DBG_LOG(LOG_ERROR, "%dth param in RPC is null", i);
            }
            return NULL;
        }
    }
    int ilast = APPEND_ENTRIES_PARAM_COUNT - 1;
    if(!params[ilast]
            || params[ilast]->type != RPC_VECTOR) {
        free(r);
        if(params[ilast]) {
            DBG_LOG(LOG_ERROR, "incorrect value in %dth param in RPC, expected = %d, got = %d",
                    (ilast), RPC_VECTOR, params[ilast]->type);
        } else {
            DBG_LOG(LOG_ERROR, "%dth param in RPC is null", ilast);
        }

        return NULL;
    }
    for(int i = 0; i < params[ilast]->length; i++) {
        struct data_t * c = params[ilast]->child[i]->child[0];
        if(!c || c->type != RPC_STRING || !c->value) {
            free(r);
            if(c) {
                DBG_LOG(LOG_ERROR, 
                        "incorrect value in %dth child param in RPC, expected = %d, got = %d",
                        (i+1), RPC_STRING, c->type);
            } else {
                DBG_LOG(LOG_ERROR, "%dth child param in RPC is null", i);
            }
            return NULL;
        }
    }

    r->term = *(json_int_t *)params[0]->value;
    r->leader_id = *(json_int_t *)params[1]->value;
    r->prev_log_index = *(json_int_t *)params[2]->value;
    r->prev_log_term = *(json_int_t *)params[3]->value;
    r->leader_commit_index = *(json_int_t *)params[4]->value;
    r->nentries = params[5]->length;
    r->entries = (struct log_entry_t **)malloc(
        sizeof(struct log_entry_t *)*r->nentries);
    if(!r->entries) {
        free(r); 
        DBG_LOG(LOG_FATAL, "memory allocation failed");
        return NULL;
    }
    for(int i = 0; i < r->nentries; i++) {
        r->entries[i] = (struct log_entry_t *)malloc(sizeof(struct log_entry_t));
        size_t outlen = 0;
        char *childval = (char *)params[5]->child[i]->child[0]->value;
        unsigned char *decoded = base64_decode(childval, strlen(childval), &outlen);
        if(!r->entries[i] || !decoded) {
            DBG_LOG(LOG_FATAL, "memory allocation failed");
            if(r->entries[i]) {
                free(r->entries[i]);
            }
            free(r);
            return NULL;
        }
        r->entries[i]->buffer = decoded;
        r->entries[i]->bufsize = outlen;
        r->entries[i]->term = *(json_int_t *)params[5]->child[i]->child[1]->value;
        r->entries[i]->index = *(json_int_t *)params[5]->child[i]->child[2]->value;
        r->entries[i]->req_id = *(json_int_t *)params[5]->child[i]->child[3]->value;
    }

    return r;

}

struct append_entries_output_t *get_append_entries_output(struct data_t *append_entries_response) {
    struct append_entries_output_t *r = (struct append_entries_output_t *)malloc(
        sizeof(struct append_entries_output_t));
    if(!r) {
        return NULL;
    }

    if(append_entries_response->length != APPEND_ENTRIES_RES_COUNT) {
        free(r);
        DBG_LOG(LOG_ERROR, "incorrect number of parameter in RPC response, expected = %d, got = %d",
                APPEND_ENTRIES_RES_COUNT, append_entries_response->length);
        return NULL;
    }

    for(int i = 0; i < APPEND_ENTRIES_RES_COUNT; i++) {
        if(!append_entries_response->child[i]
            || append_entries_response->child[i]->type != RPC_INT
            || !append_entries_response->child[i]->value) {
            free(r);
            if(append_entries_response->child[i]) {
                DBG_LOG(LOG_ERROR, "incorrect value in param in RPC, expected = %d, got = %d",
                        RPC_INT, append_entries_response->child[i]->type);
            }
            return NULL;
        }
    }

    r->term = *(json_int_t *)append_entries_response->child[0]->value;
    r->success = *(json_int_t *)append_entries_response->child[1]->value;

    return r;

}

#ifdef TEST_RAFT_RPC
int main() {
    {
        struct request_vote_input_t in = {41, 42, 43, 44};
        struct method_t *rv = make_request_vote_rpc_method(&in);
        if(rv) {
            if(strcmp(rv->name, REQUEST_VOTE_RPC)) {
                printf("failed = expected %s actual %s\n", REQUEST_VOTE_RPC, rv->name);
            } else {
                if(rv->nparams != REQUEST_VOTE_PARAM_COUNT) {
                    printf("failed = expected %d actual %d\n", REQUEST_VOTE_PARAM_COUNT, rv->nparams);
                } else {
                    if(*(json_int_t *)rv->params[0]->value != 41 ||
                            *(json_int_t *)rv->params[1]->value != 42 ||
                            *(json_int_t *)rv->params[2]->value != 43 ||
                            *(json_int_t *)rv->params[3]->value != 44) {
                        printf("failed = incorrect values in params\n");
                    }
                }
            }
            free_method_t(rv);
        } else {
            printf("failed = null method\n");
        }
    }
    {
        base64_init();
        unsigned char buf[] = {0, 41, 12, 33};
        struct log_entry_t le1 = { buf, 4, 42, 43 };
        struct log_entry_t *les[] = { &le1 };
        struct append_entries_input_t in = {41, 42, 43, 44, 45, les, 1};
        struct method_t *rv = make_append_entries_rpc_method(&in);
        if(rv) {
            if(strcmp(rv->name, APPEND_ENTRIES_RPC)) {
                printf("failed = expected %s actual %s\n", APPEND_ENTRIES_RPC, rv->name);
            } else {
                if(rv->nparams != APPEND_ENTRIES_PARAM_COUNT) {
                    printf("failed = expected %d actual %d\n", APPEND_ENTRIES_PARAM_COUNT, rv->nparams);
                } else {
                    if(*(json_int_t *)rv->params[0]->value != 41 ||
                            *(json_int_t *)rv->params[1]->value != 42 ||
                            *(json_int_t *)rv->params[2]->value != 43 ||
                            *(json_int_t *)rv->params[3]->value != 44 ||
                            *(json_int_t *)rv->params[4]->value != 45) {
                        printf("failed = incorrect values in params\n");
                    } else {
                        if(rv->params[5]->length == 1) {
                            struct data_t *dx = rv->params[5]->child[0];
                            char *d = (char *)dx->child[0]->value;
                            uint64_t term = *(json_int_t *)dx->child[1]->value;
                            uint64_t index = *(json_int_t *)dx->child[2]->value;
                            size_t outlen = 0;
                            unsigned char *buf = base64_decode(d, strlen(d), &outlen); 
                            if(outlen == 4) {
                                if(buf[0] != 0 || buf[1] != 41 || buf[2] != 12 || buf[3] != 33 || term != 42 || index != 43) {
                                    printf("failed = incorrect values in decoded buf\n");
                                }
                            } else {
                                printf("failed = incorrect values in decoded params\n");
                            }
                            if(buf) free(buf);
                        } else {
                            printf("failed = incorrect values in array params\n");
                        }
                    }
                }
            }
            free_method_t(rv);
        } else {
            printf("failed = null method\n");
        }
        base64_cleanup();
    }
    {
        struct request_vote_output_t o = {41, 42};
        struct data_t *r = make_request_vote_rpc_response(&o);
        if(r) {
            if(r->type != RPC_VECTOR || r->length != 2) {
                printf("failed = incorrect values in output\n");
            }
            free_data_t(r);
        } else {
            printf("failed = null output\n");
        }
    }
    {
        struct append_entries_output_t o = {41, 42};
        struct data_t *r = make_append_entries_rpc_response(&o);
        if(r) {
            if(r->type != RPC_VECTOR || r->length != 2) {
                printf("failed = incorrect values in output\n");
            }
            free_data_t(r);
        } else {
            printf("failed = null output\n");
        }
    }

    return 0;
}
#endif 
