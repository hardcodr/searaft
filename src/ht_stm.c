#include <stdlib.h>
#include <string.h>
#include "stm.h"
#include "dict.h"
#include "utils.h"
#include "client.h"

// copied from hiredis/net.c
static unsigned int stmHash(const void *key) {
    return dictGenHashFunction((const unsigned char *)key,
                               strlen((const char *)key));
}

static void *stmKeyDup(void *privdata, const void *src) {
    ((void) privdata);
    return u_strdup((const char *)src);
}

static void *stmValDup(void *privdata, const void *src) {
    ((void) privdata);
    return u_strdup((const char *)src);
}

static int stmKeyCompare(void *privdata, const void *key1, const void *key2) {
    int l1, l2;
    ((void) privdata);

    l1 = strlen((const char *)key1) + 1;
    l2 = strlen((const char *)key2) + 1;
    if (l1 != l2) return 0;
    return memcmp(key1, key2, l1) == 0;
}

static void stmKeyDestructor(void *privdata, void *key) {
    ((void) privdata);
    free(key);
}

static void stmValDestructor(void *privdata, void *val) {
    ((void) privdata);
    free(val);
}

static dictType stmDict = {
    stmHash,
    stmKeyDup,
    stmValDup,
    stmKeyCompare,
    stmKeyDestructor,
    stmValDestructor
};
//end copied from hiredis/net.c

struct state_machine *init_stm(struct server_context_t *s) {
    struct state_machine *stm = (struct state_machine *)malloc(sizeof(stm));
    if(stm) {
        stm->ctx = dictCreate(&stmDict, NULL);
    } else {
        return NULL;
    }
    //TODO: apply all the commited entries to the STM
    //or load from the snapshot

    return stm;
}

static char *get_str_from_offset(unsigned char *buf, int len, int offset, int *ssize) {
    *ssize = (int)buf[offset];
    if(offset + *ssize + 1 > len)
        return NULL;
    char *s = (char *)malloc(*ssize + 1);
    memcpy(s, buf + offset + 1, *ssize);
    s[*ssize] = 0;

    return s;
}

void *stm_apply(struct state_machine *stm, void *_input, int len, int *outlen) {
    unsigned char *input = (unsigned char *)_input;
    dict *d = (dict *)stm->ctx;

    int opsize;
    char *op = get_str_from_offset(input, len, 0, &opsize);
    if(!op) {
        return result_to_response(
                INCORRECT_COMMAND_ERROR, "cannot recognize command", outlen);
    }
    void *response;
    if(strcmp(op, "PUT") == 0) {
        int keysize, valsize;
        char *key = get_str_from_offset(input, len, 1 + opsize, &keysize);
        char *val = get_str_from_offset(input, len, 1 + opsize + 1 + keysize, &valsize);
        if(!key || !val) {
            response = result_to_response(
                    PARSING_ERROR, "cannot parse PUT command", outlen);
        } else {
            if(DICT_OK == dictAdd(d, key, val)) {
                response = result_to_response(SUCCESS, "", outlen);
            } else if(DICT_OK == dictReplace(d, key, val)) {
                response = result_to_response(SUCCESS, "", outlen);
            } else {
                response = result_to_response(
                    INTERNAL_ERROR, "PUT command failed", outlen);
            }
        }
        if(key) free(key);
        if(val) free(val);
    } else if(strcmp(op, "GET") == 0) {
        int keysize;
        char *key = get_str_from_offset(input, len, 1 + opsize, &keysize);
        if(!key) {
            response = result_to_response(
                    PARSING_ERROR, "cannot parse GET command", outlen);
        } else {
            printf("finding key = %s\n", key);
            dictEntry *de = dictFind(d, key);
            if(de) {
                printf("found key = %s sending response\n", key);
                response = result_to_response(SUCCESS, (char *)de->val, outlen);
            } else {
                //TODO: more graceful response
                response = result_to_response(
                    INTERNAL_ERROR, "GET command failed", outlen);
            }
            free(key);
        }
    } else if(strcmp(op, "DEL") == 0) {
        int keysize;
        char *key = get_str_from_offset(input, len, 1 + opsize, &keysize);
        if(!key) {
            response = result_to_response(
                    PARSING_ERROR, "cannot parse DEL command", outlen);
        } else {
            if(DICT_OK == dictDelete(d, key)) {
                response = result_to_response(SUCCESS, "", outlen);
            } else {
                response = result_to_response(
                    INTERNAL_ERROR, "DEL command failed", outlen);
            }
            free(key);
        }
    } else {
        char unknown[100];
        snprintf(unknown, 100, "unknown command %s", op);
        response = result_to_response(INCORRECT_COMMAND_ERROR, unknown, outlen);
    }
    free(op);
    return response;
}

void deinit_stm(struct state_machine *stm) {
    if(stm && stm->ctx) {
        dictRelease((dict *)stm->ctx);
    }
}
