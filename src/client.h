#ifndef SEARAFT_CLT_HANDLER
#define SEARAFT_CLT_HANDLER

#include "raft.h" 

#define MAX_CLIENT_CONNS (1000)

struct client_req_t {
    uint64_t req_id;
    struct client_req_t *next;
};

struct client_session_t {
    evutil_socket_t fd;
    struct bufferevent *bev;
    struct client_req_t *requests_head;
    struct client_req_t *requests_tail;
    struct client_session_t *next;
};

struct client_handler_t {
    long active_reqs; //number of current active requests
    long active_conns; //number of active connections
    struct client_session_t *clients_head;
    struct client_session_t *clients_tail;
    dict *request_to_fd;
};

enum RESULT_CODE {
   SUCCESS = 0,
   REDIRECT,
   INCORRECT_COMMAND_ERROR,
   PARSING_ERROR,
   THROTTLING_ERROR,
   INCORRECT_REQUEST_ERROR,
   INTERNAL_ERROR
};


unsigned char *result_to_response(
        int result_code, const char *buf, int *outlen);

int start_client_handler(struct server_context_t *s, int clietport);

void send_client_response(struct server_context_t *s, 
        uint64_t req_id, void *res, int reslen);

int stop_client_handler(struct server_context_t *s);

#endif
