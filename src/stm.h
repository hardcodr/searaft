#ifndef SEARAFT_STM
#define SEARAFT_STM

#include "raft.h"

struct state_machine {
    void *ctx;
};

//Initialize the STM.
//Implementation is supposed to replay all the
//commited log entries to the state machine during
//initialization.
struct state_machine *init_stm(struct server_context_t *s);

// This function applies the input to the STM and
// returns the output produced after applying the
// operation
void *stm_apply(struct state_machine *stm, void *input, int len, int *outlen);

//destory the stm
void deinit_stm(struct state_machine *stm);

#endif
