#ifndef SEARAFT_LOG
#define SEARAFT_LOG

#define MAX_CACHED_ENTRIES 100

struct log_entry_t {
    unsigned char *buffer;
    size_t bufsize;
    uint64_t term;
    uint64_t index;
    uint64_t req_id;
};

struct p_log_entry_t {
    struct log_entry_t *e;
    struct p_log_entry_t *next;
    struct p_log_entry_t *prev;
};

struct p_log {
    char *logdir;
    struct p_log_entry_t *head;
    struct p_log_entry_t *tail;
};

#define log_index(ptr) ((ptr)->e->index)
#define log_term(ptr) ((ptr)->e->term)

struct p_log *init_log(const char *logdir);

//returns the tail of the log
struct log_entry_t *get_last_entry(struct p_log *log);

//returns iterator (on which next/prev can be called) at given index
struct p_log_entry_t *get_iterator_at(struct p_log *log, uint64_t index);

//returns the log entry at the given index
struct log_entry_t *get_log_entry_at(struct p_log *log, uint64_t index);

//appends the input to tail of the current log and store it on the FS.
//retuns the log_entry which is appended on sucess; NULL otherwise
//on success the buf owenership changes to this log manager and caller
//should not free it; on error caller has to manage the buf
struct log_entry_t *append_log_entry(struct p_log *log, 
        uint64_t term, uint64_t index, uint64_t req_id,
        unsigned char *buf, size_t bufsize);

//returns the last entry appended
//the array 'entries' will be retained by the log manager on success
//, on failure it will not be
struct log_entry_t *append_log_entries(struct p_log *log, 
        struct log_entry_t **entries, int nentries);

void deinit_log(struct p_log *log);

#endif
