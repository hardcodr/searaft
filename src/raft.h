#include "rpc.h"
#include "log.h"
#include "stm.h"
#include "dict.h"

#ifndef SEARAFT_RAFT
#define SEARAFT_RAFT

enum server_state {
    FOLLOWER,
    CANDIDATE,
    LEADER
};

struct peer_t {
    int id;
    struct rpc_target *dest;
};

struct server_context_t {
    char *host;
    int port;
    int id; //allocated statically in config file
    enum server_state state;

    char *basedir;

    //// persistent state ////

    //latest term server has seen 
    //(initialized to 0 on first boot, increases monotonically)
    uint64_t current_term;
    //candidateId that received vote in current term 
    //(or null if none)
    int voted_for;
    //last unapplied index

        struct p_log *log;
    //log entries; each entry contains command for state 
    //machine, and term when entry was received by leader 
    //(first index is 1)
    struct log_entry_t *last_entry;

    //// volatile state ////
    //index of highest log entry known to be committed 
    //(initialized to 0, increases monotonically)
    uint64_t commit_index;
    uint64_t commit_term;
    //index of highest log entry applied to state machine 
    //(initialized to 0, increases monotonically)
    uint64_t log_last_applied;

    struct event_base *base;
    struct event *timer_el;
    struct event *timer_hb;
    struct timeval *election_timeout;
    struct timeval *heartbeat_timeout;
    unsigned long election_timeout_;
    unsigned long heartbeat_timeout_;

    struct state_machine *stm;

    struct rpc_context *rpc_s;
    struct rpc_context *rpc_c;

    int quoram_size;
    struct peer_t **peers;

    int current_votes;
    int current_leader;

    //for each server, index of the next log entry 
    //to send to that server.
    //(initialized to leader last log index + 1)
    uint64_t *next_index;
    //for each server, index of highest log entry 
    //known to be replicated on server.
    //(initialized to 0, increases monotonically)
    uint64_t *match_index; 

    //time since last heartbeat or append entries sent
    struct timeval *last_heartbeat_at;

    struct client_handler_t *client_handler;
};

struct server_context_t *init_raft_server(int id, const char *basedir,
    const char *host, int port, enum rpc_proto proto, 
    long election_timeout, long heartbeat_timeout);

int send_append_entries(struct server_context_t *s, 
        uint64_t start_index, int peer_index);

/* logs the entry to disk and send it to all the clients.
 * Returns the index of the log entry, 0 if its no longer 
 * server and -1 in case of errors
 */
uint64_t log_and_send_append_entries(struct server_context_t *s, 
        uint64_t req_id, unsigned char *buf, size_t bufsize);

int add_pairs(struct server_context_t *s,  
        int npairs, int id[], const char *host[], int port[]);

int start_server(struct server_context_t *s, int clientport);

void deinit_raft_server(struct server_context_t *context);

#endif
