
#ifndef SEARAFT_RAFT_RPC
#define SEARAFT_RAFT_RPC

#include <jansson.h>

#include "log.h"
#include "rpc.h"

#define REQUEST_VOTE_RPC "RequestVote"
#define APPEND_ENTRIES_RPC "AppendEntries"

#define MAX_APPEND_ENTRIES 50

struct request_vote_input_t {
    uint64_t term; //candidate term
    uint32_t candidate_id;
    uint64_t last_log_index; //index of candidate's last log entry (§5.4) 
    uint64_t last_log_term; //term of candidate's last log entry (§5.4)
};

struct request_vote_output_t {
    uint64_t term; // currentTerm, for candidate to update itself
    uint32_t vote_granted; //1 - means vote granted, 0 - means no
};

struct append_entries_input_t {
    uint64_t term;       //leader's term
    uint32_t leader_id;  //so follower can redirect clients
    uint64_t prev_log_index; //index of log entry immediately preceding new ones
    uint64_t prev_log_term;  //term of prevLogIndex entry
    uint64_t leader_commit_index;  //leader's commitIndex
    struct log_entry_t **entries;  //log entries to store (empty for heartbeat;
        //may send more than one for efficiency)
    int nentries;
};

struct append_entries_output_t {
    uint64_t term; //currentTerm, for leader to update itself
    uint32_t success; //true if follower contained entry matching 
        //prevLogIndex and prevLogTerm
};

struct method_t *make_request_vote_rpc_method(struct request_vote_input_t *input);

struct data_t *make_request_vote_rpc_response(struct request_vote_output_t *output);

struct method_t *make_append_entries_rpc_method(struct append_entries_input_t *input);

struct data_t *make_append_entries_rpc_response(struct append_entries_output_t *output);

struct request_vote_input_t *get_request_vote_input_params(struct data_t *params[], int nparams);

struct request_vote_output_t *get_request_vote_output(struct data_t *request_vote_response);

struct append_entries_input_t *get_append_entries_input_params(struct data_t *params[], int nparams);

struct append_entries_output_t *get_append_entries_output(struct data_t *append_entries_response);

#endif
