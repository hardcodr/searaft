1. Candidate does not leave candidancies if there are only two nodes
   and the timeout are such that votes split always equally. Then third 
   node gets added and it chooses one of the existing node. The other node
   since is on same term, it does not leave candidancy since the code that
   resets the state is executed only when its_term < other_term. The fix
   is straighforward if we add check (state == FOLLOWER) when his_term >= 
   other_term, but then that code will be always executed un-necesarily. 
   This should be handled during election. During election check how many
   negative votes the candidate gets, if its less than quoram size then step
   down as the CANDIDATE and become follower.
